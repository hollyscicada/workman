package com.kb.api.persistence;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.kb.api.util.Common;


@Repository
public class MasterDao {
	
	@Autowired
	@Qualifier("sqlSessionMaster")
	SqlSession sqlSessionMaster;
	
	
	
	@Autowired 
	@Qualifier("sqlSessionSlave")
	SqlSession sqlSessionSlave;
	
	@Autowired 
	@Qualifier("sqlSessionTran")
	SqlSession sqlSessionTran;
	
	
	public List<?> dataList(String mapperNm, String queryNm, Object vo){
		return sqlSessionSlave.selectList(mapperNm+"."+queryNm,vo);
	}
	public Object dataRead(String mapperNm, String queryNm, Object vo){
		return sqlSessionSlave.selectOne(mapperNm+"."+queryNm,vo);
	}
	public int dataCreate(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.insert(mapperNm+"."+queryNm,vo);
	}
	public int dataDelete(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.update(mapperNm+"."+queryNm,vo);
	}
	public int dataUpdate(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.update(mapperNm+"."+queryNm,vo);
	}
	public int dataCount(String mapperNm, String queryNm, Object map) {
		return sqlSessionMaster.selectOne(mapperNm+"."+queryNm,map);
	}
	
	public List<?> dataListTras(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.selectList(mapperNm+"."+queryNm,vo);
	}
	public List<Object> dataListTra(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.selectList(mapperNm+"."+queryNm,vo);
	}
	public Object dataReadTra(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.selectOne(mapperNm+"."+queryNm,vo);
	}
	public int dataCreateTra(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.insert(mapperNm+"."+queryNm,vo);
	}
	public int dataDeleteTra(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.update(mapperNm+"."+queryNm,vo);
	}
	public int dataUpdateTra(String mapperNm, String queryNm, Object vo){
		return sqlSessionTran.update(mapperNm+"."+queryNm,vo);
	}
	public int dataCountTra(String mapperNm, String queryNm, Object map) {
		return sqlSessionTran.selectOne(mapperNm+"."+queryNm,map);
	}
}

