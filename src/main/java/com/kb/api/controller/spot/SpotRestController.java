package com.kb.api.controller.spot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common; 
import com.kb.api.util.ProHashMap;

@RestController          
public class SpotRestController {
	
	@Inject MasterService masterService;  
	
	@PostMapping("/api/v1/spot/list") 
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
	  	ResponseEntity<Map<String, Object>> entity = null;         
		Map<String, Object> map = new HashMap<String, Object>();  
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block());   
  
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCount("mapper.SpotMapper", "list_cnt", paramMap); 
		List<HashMap> list = (List<HashMap>) masterService.dataList("mapper.SpotMapper", "list", paramMap); 
		
		 
		
		param.setTotalCount(record); // 게시물 총 개수 
		map.put("success", true);
		map.put("paging", param.getPageIngObj());	
		map.put("data", list); 
		
     		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/spot/read")    
	public ResponseEntity<Map<String, Object>> read(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;   
		Map<String, Object> map = new HashMap<String, Object>();
		 
  
		HashMap paramMap = new HashMap(param);    
		   
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.SpotMapper", "read", paramMap);
		map.put("success", true);  
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("read", read);
		map.put("data", data);
		 
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/spot/update")  
	public ResponseEntity<Map<String, Object>> removeDELETE(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCreate("mapper.SpotMapper", "update", paramMap);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
}
