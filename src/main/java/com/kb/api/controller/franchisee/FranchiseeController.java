package com.kb.api.controller.franchisee;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FranchiseeController {  
	@GetMapping(value ={ "/supervise/franchisee" })
	public String franchisee(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/franchisee/franchisee";
	}   
}
