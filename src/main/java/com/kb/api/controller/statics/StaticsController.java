package com.kb.api.controller.statics;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StaticsController {  
	@GetMapping(value ={ "/supervise/statics/1" })
	public String static_1(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/statics/static_1";
	}   
	
	@GetMapping(value ={ "/supervise/statics/2" })
	public String static_2(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/statics/static_2";
	}  
}
