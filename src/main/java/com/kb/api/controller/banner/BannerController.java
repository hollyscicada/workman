package com.kb.api.controller.banner;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kb.api.service.MasterService;

@Controller  
public class BannerController {  
	
	@Inject MasterService masterService;    
	 
	@GetMapping(value ={ "/supervise/banner" })
	public String notice(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/banner/banner";
	}   
	@GetMapping(value ={ "/supervise/banner/write" })
	public String notice_write(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/banner/banner_write";
	} 
	@GetMapping(value ={ "/supervise/banner/update/{id}" })
	public String notice_write(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String id) throws Exception {
		
		
		HashMap<String, Object> readMap = new HashMap<String, Object>();
		readMap.put("id", id);
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.BannerMapper", "read", id);
		model.addAttribute("id", id);
		model.addAttribute("read", read);
		  
		return "adminLayout/banner/banner_update";
	}   
}
