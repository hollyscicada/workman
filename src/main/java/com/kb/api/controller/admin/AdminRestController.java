package com.kb.api.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common; 
import com.kb.api.util.ProHashMap;

@RestController 
public class AdminRestController {
	
	@Inject MasterService masterService;  
	 
	@PostMapping("/api/v1/admin/list") 
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();   
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 
  
		HashMap paramMap = new HashMap(param); 
		
		int listSize = masterService.dataCount("mapper.AdminMapper", "list_cnt", paramMap);  
		List<HashMap> list = (List<HashMap>) masterService.dataList("mapper.AdminMapper", "list", paramMap);
		
		
		 
		param.setTotalCount(listSize); // 게시물 총 개수 
		map.put("success", true);
		map.put("paging", param.getPageIngObj());	  
		 
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("listSize", listSize);
		data.put("list", list);
		map.put("data", data);
		
     		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	@PostMapping("/api/v1/admin/read")    
	public ResponseEntity<Map<String, Object>> read(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;   
		Map<String, Object> map = new HashMap<String, Object>();
		 
  
		HashMap paramMap = new HashMap(param);    
		   
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.AdminMapper", "read", paramMap);
		map.put("success", true);  
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("read", read);
		map.put("data", data);   
		 
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	

	@PostMapping("/api/v1/admin/update")
	public ResponseEntity<Map<String, Object>> update(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCreate("mapper.AdminMapper", "update", paramMap);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	@PostMapping("/api/v1/admin/update/pw")
	public ResponseEntity<Map<String, Object>> updatePw(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCreate("mapper.AdminMapper", "update_pw", paramMap);
		map.put("success", false);  
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	@PostMapping("/api/v1/admin/create")
	public ResponseEntity<Map<String, Object>> create(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		int record = masterService.dataCreate("mapper.AdminMapper", "create", paramMap);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
}
