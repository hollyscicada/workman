package com.kb.api.controller.setting;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kb.api.service.MasterService;

@Controller
public class SettingController {  
	
	@Inject MasterService masterService;    
	
	@GetMapping(value ={ "/supervise/setting/fees" })
	public String setting_fees(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/setting/setting_fees";
	}   
	@GetMapping(value ={ "/supervise/setting/manager/create" })
	public String setting_manager_sub1(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/setting/setting_manager_create";
	}   
	@GetMapping(value ={ "/supervise/setting/manager/update/{id}" })
	public String manager_sub2(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String id) throws Exception {
		
		HashMap<String, Object> readMap = new HashMap<String, Object>();
		readMap.put("id", id);
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.AdminMapper", "read", id);
		model.addAttribute("id", id);
		model.addAttribute("read", read);
		
		return "adminLayout/setting/setting_manager_update";
	}   
	@GetMapping(value ={ "/supervise/setting/manager" })
	public String setting_manager(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/setting/setting_manager";
	}   
	@GetMapping(value ={ "/supervise/setting/punish" })
	public String setting_punish(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/setting/setting_punish";
	}   
}
