package com.kb.api.controller.income;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IncomeController {  
	@GetMapping(value ={ "/supervise/income" })
	public String income(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/income/income";
	}   
}
