package com.kb.api.controller.login;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import com.kb.api.service.MasterService;


@Controller
public class LoginController {
	@Inject MasterService masterService;
//	@Inject DashboardService DashboardService;

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	/*관리자 현황판*/
	@GetMapping(value ={ "" })
	public String main(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "redirect:/supervise";
	}   
	/*관리자 현황판*/
	@GetMapping(value ={ "/supervise" })
	public String dashboard(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		
		if(!StringUtils.isEmpty(session.getAttribute("MEMBER"))) {   
			return "redirect:/supervise/info";
		}else {
			return "adminLayoutNotLogin/login";
		}
	}   
	/**
	 * @msg : 로그아웃
	 */
	@GetMapping(value = {"/supervise/logout"})
	public String logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		return "redirect:/supervise";
	}
}
