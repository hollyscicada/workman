package com.kb.api.controller.login;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class LoginRestController {
	
	@Inject MasterService masterService;
	
	/*관리자 로그인*/
	@PostMapping("/api/v1/login")  
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) masterService.dataRead("mapper.LoginMapper", "adminLogin", paramMap);
     		
		if (retMap != null) {
			session.setAttribute("MEMBER", retMap);
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
