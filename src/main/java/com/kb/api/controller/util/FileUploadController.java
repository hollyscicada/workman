package com.kb.api.controller.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


/**
 * @version : java1.8 
 * @author : ohs
 * @date : 2018. 1. 17.
 * @msg : 파일 업로드 하는 함수들 바이너리형식으로 요청, multipart-form-data 형식  두가지 지원가능
*/
@Controller
@RequestMapping("/api/file/*")
public class FileUploadController {
	String setCharEncoding = "UTF-8";
	
	
	/**
	 * @msg : 업로드 관련 path 모음
	 */
	public String UPLOAD_PATH = "/resources/images/upload/";
	
	
	@Value("#{props['domain.url']}")
	String domain_url;
	
	public String fileupload(MultipartFile file1, HttpServletRequest request) { 
		try{
			if(!file1.isEmpty()){ //첨부파일이 존재하면
				
				String fileName=""; 
				String strToday = simpleDate("yyyyMMddkms");
				String path = request.getServletContext().getRealPath(UPLOAD_PATH);
				
				//첨부파일의 이름 
				fileName=file1.getOriginalFilename();
				String ext = fileName.substring(fileName.lastIndexOf(".") + 1);//확장자 구하기
				//파일명이 한글명일경우 UUID객체 이용하여 유니크값으로 파일명을 대체한다.
				if(checkLang(fileName)){
					fileName = UUID.randomUUID().toString().replace("-", "")+"."+ext;
				}
				fileName = strToday+"_"+fileName;
				file1.transferTo(new File(path+fileName));
				return domain_url+UPLOAD_PATH+fileName;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param realPath : 해당 경로에 디렉토리가 있다면 넘어가고 없다면 디렉토리를 생성한다.
	 */
	public void mkDir(String realPath){
		File updir = new File(realPath);
		if (!updir.exists()) updir.mkdirs();
	}
	
	/**
	 * 
	 * @message : 포맷패턴을 넘겨주면 그에 맞게 날짜형식을 리턴한다.
	 * @param fommat : 포맷패턴
	 * @return : 포맷에 따른 날짜를 String형식으로 리턴
	 */
	public String simpleDate(String fommat){
		SimpleDateFormat sdf = new SimpleDateFormat(fommat);
		Calendar c1 = Calendar.getInstance();
		String strToday = sdf.format(c1.getTime());
		return strToday;
	}
	
	/**
	 * @msg : 해당 문자열에 한글이 포함됬는지 확인 함수
	 * @param str : 검사할 문자열
	 * @return : true 한글존재, false 한글 없음
	 */
	public boolean checkLang(String str){
		if(str.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * @msg : 파일업로드 하는 객체이며 multipart 형식으로 넘어왔을 때만 사용하는 객체이며 thumb를 만든다. 또한 위의 객체와 리턴방식이 다름
	 */
	@PostMapping(value = "/ajaxupload") // thumb 도 만들기
	public @ResponseBody String ajaxupload(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) throws Exception {
//		response.setCharacterEncoding(setCharEncoding);
		response.setContentType("text/html; charset="+setCharEncoding+" \" ");
		
		String realPath = request.getServletContext().getRealPath(UPLOAD_PATH);
		String filename = multipart_upload(request, realPath);
		return domain_url+UPLOAD_PATH+filename;
	}
	
	/**
	 * @message : 실질적인 업로드하는 객체
	 * @param request : 업로드할 객체를 가지고 있는 요청데이터
	 * @param realPath : 실질적으로 사진을 업로드할 경고
	 * @return : 이미지 업로드 후 업로드한 파일명
	 */
	public String multipart_upload(HttpServletRequest request, String realPath) throws IllegalStateException, IOException{
		mkDir(realPath);
		String filename = "";
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		MultipartFile mrequest = null;
//		String strToday = simpleDate("yyyyMMddkms");
		
		while(iterator.hasNext()){
			mrequest = multipartHttpServletRequest.getFile(iterator.next());
			
			String filenameTemp = mrequest.getOriginalFilename();
			String ext = filenameTemp.substring(filenameTemp.lastIndexOf(".") + 1); //확장자 구하기
			filename += UUID.randomUUID().toString().replace("-", "")+"."+ext;
			if(mrequest.isEmpty() == false) mrequest.transferTo(new File(realPath + filename));
		}
		return filename;
	}
	
}
