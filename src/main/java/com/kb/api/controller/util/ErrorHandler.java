package com.kb.api.controller.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {
	/*
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> globalExceptionHandler(Exception ex, WebRequest request) {
		ResponseEntity<Object> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		map.put("erroMsg", "시스템 오류가 발생하였습니다. 잠시후 다시 시도해주세요.");

		entity = new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		return entity;
	}
	*/
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> globalExceptionHandler(Exception ex, WebRequest request) {
		ResponseEntity<Object> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		map.put("errorCode", "JWT");
		map.put("erroMsg", "유효한 접근이 아닙니다. 다시 로그인해주세요.");
		
		entity = new ResponseEntity<Object>(map, HttpStatus.OK);
		return entity;
	}
}
