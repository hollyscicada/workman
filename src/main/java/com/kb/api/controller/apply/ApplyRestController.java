package com.kb.api.controller.apply;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.service.info.InfoService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class ApplyRestController {
	
	@Inject MasterService masterService;  
	@Inject InfoService infoService;  
	
	@PostMapping("/api/v1/apply/list") 
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();   
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 
  
		HashMap paramMap = new HashMap(param);
		
		int listSize = masterService.dataCount("mapper.ApplyMapper", "list_cnt", paramMap); 
		int noRequestSize = masterService.dataCount("mapper.ApplyMapper", "noRequestSize", paramMap); 
		int yesRequestSize = masterService.dataCount("mapper.ApplyMapper", "yesRequestSize", paramMap); 
		List<HashMap> list = (List<HashMap>) masterService.dataList("mapper.ApplyMapper", "list", paramMap);
		
		
		 
		param.setTotalCount(listSize); // 게시물 총 개수 
		map.put("success", true);
		map.put("paging", param.getPageIngObj());	 
		 
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("listSize", listSize);   
		data.put("noRequestSize", noRequestSize);
		data.put("yesRequestSize", yesRequestSize);
		data.put("list", list);
		map.put("data", data);
		
     		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/apply/read")    
	public ResponseEntity<Map<String, Object>> read(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		  
  
		HashMap paramMap = new HashMap(param);   
		   
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.ApplyMapper", "read", paramMap);
		map.put("success", true);  
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("read", read);
		map.put("data", data);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	@PostMapping("/api/v1/apply/info/update")  
	public ResponseEntity<Map<String, Object>> update(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		map = infoService.applyUpdate(paramMap, map);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	@PostMapping("/api/v1/apply/assign")  
	public ResponseEntity<Map<String, Object>> assign(@RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param); 
		
		map = infoService.applyAssign(paramMap, map);    
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	
}
