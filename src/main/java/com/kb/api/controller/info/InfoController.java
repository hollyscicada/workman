package com.kb.api.controller.info;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.kb.api.service.MasterService;

@Controller
public class InfoController {
	
	@Inject MasterService masterService;
	@GetMapping(value ={ "/supervise/info" })
	public String request(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/info/info";
	}  
}
