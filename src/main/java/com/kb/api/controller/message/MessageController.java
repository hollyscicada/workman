package com.kb.api.controller.message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MessageController {  
	@GetMapping(value ={ "/supervise/message" })
	public String message(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/message/message";
	}   
	@GetMapping(value ={ "/supervise/message/send" })
	public String message_send(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/message/message_send";
	}   
}
 