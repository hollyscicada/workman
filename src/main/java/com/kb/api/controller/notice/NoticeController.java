package com.kb.api.controller.notice;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kb.api.service.MasterService;

@Controller
public class NoticeController {  
	
	@Inject MasterService masterService;    
	
	@GetMapping(value ={ "/supervise/notice" })
	public String notice(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/notice/notice";
	}   
	@GetMapping(value ={ "/supervise/notice/write" })
	public String notice_write(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/notice/notice_write";
	} 
	@GetMapping(value ={ "/supervise/notice/update/{id}" })
	public String notice_write(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String id) throws Exception {
		
		
		HashMap<String, Object> readMap = new HashMap<String, Object>();
		readMap.put("id", id);
		HashMap<String, Object> read = (HashMap<String, Object>) masterService.dataRead("mapper.NoticeMapper", "read", id);
		model.addAttribute("id", id);
		model.addAttribute("read", read);
		  
		return "adminLayout/notice/notice_update";
	}   
}
