package com.kb.api.controller.workman;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WorkmanController {  
	@GetMapping(value ={ "/supervise/workman" })
	public String workman(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayout/workman/workman";
	}   
}
