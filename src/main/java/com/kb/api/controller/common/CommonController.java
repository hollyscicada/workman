package com.kb.api.controller.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CommonController {  
	@GetMapping(value ={ "/supervise/common/popup" })
	public String income(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "adminLayoutNotLogin/common/img_popup";
	}   
}
