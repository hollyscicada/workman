package com.kb.api.controller.common;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.model.SmsModel;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.SmsSendUtil;
import com.kb.api.util.Util;

@RestController 
public class CommonRestController {
	
	@Inject MasterService masterService;  
	@Inject SmsSendUtil smsSendUtil;
	
	
	/**
	 * 휴대폰으로 인증번호 보내기 
	 */
	@PostMapping("/api/v1/member/auth/rand/number")  
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		String rand = Util.makeAuthRandNumber();
		
		Map<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("TARGET_ADDRESS", paramMap.get("phone").toString());
		dataMap.put("TITLE", "[일꾼] 인증번호가 발송되었습니다.");
		dataMap.put("CONTENTS", "[일꾼] 인증번호 : "+rand);
		SmsModel smsModel = null;
		smsModel = smsSendUtil.smsSend(dataMap);
		
		
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("authNumber", rand);
		map.put("data", retMap);
		
		if(smsModel != null && smsModel.getResult_code() != null && smsModel.getResult_code().equals("1")) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	@PostMapping("/api/v1/common/remove") 
	public ResponseEntity<Map<String, Object>> remove(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataCreate("mapper.CommonMapper", "remove", param);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	@PostMapping("/api/v1/common/file/get") 
	public ResponseEntity<Map<String, Object>> fileGet(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap<String, Object> rstMap = null;
		rstMap = (HashMap<String, Object>) masterService.dataRead("mapper.CommonMapper", "fileGet", param);
		
		map.put("success", false);
		if(rstMap != null) {
			map.put("success", true);
		}
		
		map.put("data", rstMap);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		 
		return entity;
	}
	
	@PostMapping("/api/v1/common/file/save") 
	public ResponseEntity<Map<String, Object>> fileSave(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		int record = masterService.dataCreate("mapper.CommonMapper", "fileSave", param);
		map.put("success", false);
		if(record > 0) map.put("success", true);
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	
}
