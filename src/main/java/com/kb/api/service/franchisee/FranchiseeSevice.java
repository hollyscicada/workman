package com.kb.api.service.franchisee;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.kb.api.persistence.MasterDao;

@Service
public class FranchiseeSevice {
	
	@Inject MasterDao masterDao;   
  
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> update(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);

		
		
		int record = masterDao.dataCreateTra("mapper.WorkmanMapper", "franchiseeUserUpdate", paramMap);
		
		if(record>0) {
			
			record += masterDao.dataCreateTra("mapper.FranchiseeMapper", "update", paramMap);
		}
		
		if(record == 2) {
			map.put("success", true);
		}else {
			throw new Exception();
		}
		return map;
	}
	
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> remove(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);

		
		
		HashMap<String, Object> read = null;
		read = (HashMap<String, Object>) masterDao.dataRead("mapper.FranchiseeMapper", "read", paramMap);
		
		if(read != null) {
			HashMap<String, Object> workmanMap = new HashMap<String, Object>();
			workmanMap.put("table_name", "user");
			workmanMap.put("id", read.get("user_id"));
			int record = masterDao.dataCreateTra("mapper.CommonMapper", "remove", workmanMap);
			if(record > 0) {
				map.put("success", true);
			}
		}else {
			throw new Exception();
		}
		return map;
	}
}
