package com.kb.api.service.info;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kb.api.persistence.MasterDao;

@Service
public class InfoService {
	
	@Inject MasterDao masterDao;   
  
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> requestUpdate(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);
		
		
		
		int record = masterDao.dataCreateTra("mapper.RequestMapper", "infoUpdate", paramMap);
		
		if(record>0) {
			map.put("success", true);
		}else {
			throw new Exception();
		}
		
		return map;
	}
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> applyUpdate(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);

		
		
		int record = masterDao.dataCreateTra("mapper.ApplyMapper", "infoUpdate", paramMap);
		
		if(record>0) {
			record += masterDao.dataCreateTra("mapper.WorkmanMapper", "phoneUpdate", paramMap);
		}
		
		if(record == 2) {
			map.put("success", true);
		}else {
			throw new Exception();
		}
		return map;
	}
	
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> requestAssign(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);
		
		
		
		int record = masterDao.dataCreateTra("mapper.RequestMapper", "assign", paramMap);
		
		if(record>0) {
			record += masterDao.dataCreateTra("mapper.RequestMapper", "assign_status_update", paramMap);
		}
		
		if(record == 2) {
			map.put("success", true);
		}else {
			throw new Exception();
		}
		return map;
	}
	@Transactional(value = "transactionManagerTran", rollbackFor = Exception.class )
	public Map<String, Object> applyAssign(HashMap<String, Object> paramMap, Map<String, Object> map) throws Exception{
		map.put("success", false);
		
		
		
		int record = masterDao.dataCreateTra("mapper.ApplyMapper", "assign", paramMap);
		
		if(record>0) {
			record += masterDao.dataCreateTra("mapper.ApplyMapper", "assign_status_update", paramMap);
		}
		
		if(record == 2) {
			map.put("success", true);
		}else {
			throw new Exception();
		}
		return map;
	}
}
