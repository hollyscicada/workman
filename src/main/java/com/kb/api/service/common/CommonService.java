package com.kb.api.service.common;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.kb.api.persistence.MasterDao;
import com.kb.api.util.Common;
import com.kb.api.util.Util;

@Service
public class CommonService {
	
	@Inject MasterDao masterDao; 
	public void commonSession(HashMap<String, Object> paramMap, HttpSession session, String isLogin) {
		HashMap<String, Object> retMap = null;
		if(Util.chkNullData(isLogin, "Y")) {
			retMap = (HashMap) masterDao.dataRead("mapper.front.LoginMapper", "user_info_id", paramMap);
		}else {
			retMap = (HashMap) masterDao.dataRead("mapper.front.LoginMapper", "user_info_seq", paramMap);
		}
		retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
		retMap.put("custom_pw", retMap.get("custom_pw"));
		session.setAttribute("MEMBER", retMap);
	}
	
}
