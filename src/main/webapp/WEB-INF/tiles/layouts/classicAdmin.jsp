<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>일꾼 관리자시스템</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="format-detection" content="telephone=no" />
    
	<script src="/resources/js/jquery.min.js"></script>
	<script src="/resources/js/commonAjax.js"></script>
	<script src="/resources/js/commonData.js"></script>
	<script src="/resources/js/common.js"></script>
	<script src="https://www.jsviews.com/download/jsrender.js"></script>
    


 	<link rel="stylesheet" href="/resources/css/common.css" />
    <link rel="stylesheet" href="/resources/css/header.css" />
</head>

<body>
    <!-- Main wrapper  -->
		<tiles:insertAttribute name="headerAdmin" />
		<tiles:insertAttribute name="contentAdmin" />
		<tiles:insertAttribute name="footerAdmin" />
</body>

</html>
