<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/jsrender.jsp"%>
<style>

</style>
<script>
	$(function(){
		var global_menu_tab = $(".global-menu-tab");
		for(var i = 0 ; i < global_menu_tab.length ; i++){
			var data_url = global_menu_tab.eq(i).attr("data-url").split("|");
			for(var x = 0 ; x < data_url.length ; x++){
				if(location.pathname.indexOf(data_url[x]) > -1 && data_url[x]){
					global_menu_tab.removeClass("active");
					global_menu_tab.eq(i).addClass("active");
				}
			}
		}
	})
</script>
<header>
	<div class="top_wrap contents_width">
		<div class="top_inner">
			<div class="logo">
				<img src="/resources/images/logo.png" />
			</div>
			<div class="login_box">
				<span class="bold">${sessionScope.MEMBER.name }</span> 
					<a class="logout_btn" href="/supervise/logout">로그아웃</a>
			</div>
		</div>
	</div>

	<div class="main_menu_wrap">
		<div class="main_menu">
			<ul class="menu_left">
				<li class="global-menu-tab" data-url="info"><a href="/supervise/info"><span>Today</span></a></li>
				<li class="global-menu-tab" data-url="request"><a href="/supervise/request"><span>요청</span></a></li>
				<li class="global-menu-tab" data-url="apply"><a href="/supervise/apply"><span>지원</span></a></li>
				<li class="global-menu-tab" data-url="franchisee"><a href="/supervise/franchisee"><span>가맹점</span></a></li>
				<li class="global-menu-tab" data-url="workman"><a href="/supervise/workman"><span>일꾼</span></a></li>
				<li class="global-menu-tab" data-url="spot"><a href="/supervise/spot"><span>지점</span></a></li>
				<li class="global-menu-tab" data-url="income"><a href="/supervise/income"><span>매출</span></a></li>
				<li class="global-menu-tab" data-url="statics"><a href="/supervise/statics/1"><span>통계</span></a></li>
				<li class="global-menu-tab" data-url="notice"><a href="/supervise/notice"><span>공지사항</span></a></li>
				<li class="global-menu-tab" data-url="banner"><a href="/supervise/banner"><span>배너관리</span></a></li>
			</ul>
			<ul class="menu_right">
				<li class="message global-menu-tab" style="width:28px !important" data-url="message"><a href="/supervise/message"></a></li>
				<li class="setting global-menu-tab" style="width:28px !important" data-url="setting"><a href="/supervise/setting/fees"></a></li>
			</ul>
		</div>
	</div>
</header>
