<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
$.views.converters("job_format",
        function(val) {
            var job_names = "";
                if(val || val == 0){
                    job_names = globalJobArray[val]
                }

              return job_names;
        }
    );
    
    
$.views.converters("job_format_multi",
        function(val) {

            var job_names = "";
            if(val && val != '[]' && val != 'undefined' && val.indexOf('null')<=-1){
                var job_json = JSON.parse(val);

                for(var i = 0 ; i < job_json.length ; i++){
                    var index = job_json[i];
                    var job_name = globalJobArray[index];
                    job_names += job_name;

                    if(i != job_json.length-1){
                        job_names += ", ";
                    }
                }
            }

          return job_names;
        }
      );

    $.views.converters("comma",
        function(val) {
            val = String(val);
            return val.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

        }
    );
    $.views.converters("hipen",
        function(val) {
    			if(val){
    				val = String(val);
    				return val.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
    			}else{
    				return "";
    			}
        }
    );
</script>
<script id="apply_list" type="text/x-jsrender">
      {{for list}}
				<tr class="applyRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{job_format:job_category}}</td>
                    <td>
						{{if status_flag == 0}}
                   			 대기중
                		{{else}}
                    		배정완료
                		{{/if}}
					</td>
                    <td>{{comma:pay}}</td>
                    <td>{{hipen:user_phone}}</td>
                    <td>{{:apply_date}}</td>
 					<td></td>
                    <td>{{:accept_date}}</td>
                    <td>{{:end_date}}</td>
                    <td>{{:user_name}}</td>
                    <td>{{hipen:user_phone}}</td>
                    <td>{{:branch_name}}</td>
                    <td>{{hipen:branch_tel}}</td>
                    <td>결제방식</td>
                </tr>
     {{/for}}
</script>
<script id="request_list" type="text/x-jsrender">
      {{for list}}
				<tr class="requestRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{:shop_name}}</td>
                    <td>{{job_format:job_category}}</td>
                    <td>
						{{if status_flag == 0}}
                   			 대기중
                		{{else}}
                    		배정완료
                		{{/if}}
					</td>
                    <td>{{comma:pay}}</td>
                    <td>{{hipen:shop_tel}}</td>
                    <td>{{:apply_date}}</td>
                    <td></td>
                    <td>{{:accept_date}}</td>
                    <td>{{:end_date}}</td>
                    <td>{{:user_name}}</td>
                    <td>{{hipen:user_phone}}</td>
                    <td>{{:location}}({{:sub_location}})</td>
                    <td>{{:branch_name}}</td>
                    <td>{{hipen:branch_tel}}</td>
                    <td>결제방식</td>
                </tr>
     {{/for}}
</script>
<script id="info_apply_list" type="text/x-jsrender">
      {{for list}}
				<tr class="applyRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>
						{{if status_flag == 0}}
                   			 대기중
                		{{else}}
                    		배정완료
                		{{/if}}
					</td>
                    <td>{{comma:pay}}</td>
                    <td>{{:apply_date}}</td>
                    <td>{{:accept_date}}</td>
                    <td>{{:user_name}}</td>
                    <td>{{hipen:user_phone}}</td>
                </tr>
     {{/for}}
</script>
<script id="info_request_list" type="text/x-jsrender">
      {{for list}}
				<tr class="requestRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{:shop_name}}</td>
                    <td>
						{{if status_flag == 0}}
                   			 대기중
                		{{else}}
                    		배정완료
                		{{/if}}
					</td>
                    <td>{{:apply_date}}</td>
                    <td>{{:accept_date}}</td>
                    <td>{{:user_name}}</td>
                    <td>{{hipen:user_phone}}</td>
                </tr>
     {{/for}}
</script>
<script id="work_man" type="text/x-jsrender">
      {{for data}}


				<tr class="workmanRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{:email}}</td>
                    <td>{{:name}}</td>
                    <td>{{job_format_multi:job_json}}</td>
                    <td>
						{{if status_flag == 0}}
                   			 미승인
                		{{else}}
                    		승인
                		{{/if}}
					</td>
                    <td>{{hipen:phone}}</td>
                    <td>{{:user_location}}</td>
                    <td>{{:register_count}}</td>
            		<td>{{:accept_count}}</td>
            		<td>{{:cancel_count}}</td>
            		<td>{{:absent_count}}</td>
                    <td>
							 {{if type_flag == 0}}
                   				 일반
                			{{else}}
                    			점주
                			{{/if}}
					</td>
					<td>{{:prepaid_flag}}</td>
            		<td>{{:branch_name}}</td>
            		<td>{{:create_date}}</td>
                </tr>
     {{/for}}
</script>
<script id="franchisee_list" type="text/x-jsrender">
      {{for list}}

					<tr class="franchiseeRead" data-id="{{:id}}">
                        <td>{{:rownum}}</td>
            			<td>{{:id}}</td>
            			<td>{{:name}}</td>
            			<td>{{:email}}</td>
            			<td>{{:user_name}}</td>
                        <td>업종업종</td>
                        <td> 
							{{if status_flag == 0}}
                    			미승인
                			{{else}}
                    			승인
                			{{/if}}
						</td>
                        <td>{{hipen:tel}}</td>
            			<td>{{hipen:phone}}</td>
                        <td>{{:location}}({{:sub_location}})</td>

                        <td>{{:register_count}}</td>
           				<td>{{:accept_count}}</td>
            			<td>{{:cancel_count}}</td>

                        <td>미납(?)</td>
                        <td>현금(?)</td>
						
						<td>{{:cash}}</td>
                        <td>{{:branch_name}}</td>
                        <td>{{hipen:branch_tel}}</td>
                    </tr>
     {{/for}}
</script>    
<script id="spot_list" type="text/x-jsrender">
      {{for data}}

					<tr class="spotRead" data-id="{{:id}}">
                        <td>{{:rownum}}</td>
                        <td>{{:id}}</td>
                        <td>{{:name}}</td>
                        <td>{{:ceo_name}}</td>
                        <td>{{hipen:tel}}</td>
                        <td>{{hipen:phone}}</td>
                        <td>{{:location}}</td>
                        <td>{{:shop_cnt}}</td>
                        <td>000000</td>
                        <td>000000</td>
                        <td>{{:business_code}}</td>
                        <td>100%</td>
                        <td>{{:user_cnt}}</td>
                    </tr>

     {{/for}}
</script>    
<script id="notice_list" type="text/x-jsrender">
      {{for list}}

				 <tr class="noticeRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:title}}</td>
                    <td>{{:admin_name}}</td>
                    <td>{{:create_date}}</td>
                    <td class="delete_btn">
                        <input type="button" class="update-btn" value="수정"/> / 
                        <input type="button" class="delete-btn" value="삭제"/>
                    </td>
                </tr>


     {{/for}}
</script>    
<script id="banner_list" type="text/x-jsrender">
      {{for list}}

				 <tr class="bannerRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{:file_url}}</td>
                    <td>{{:sort}}</td>
                    <td>{{:callback_url}}</td>
                    <td>{{:create_date}}</td>
                    <td class="delete_btn">
                        <input type="button" class="update-btn" value="수정"/> / 
                        <input type="button" class="delete-btn" value="삭제"/>
                    </td>
                </tr>


     {{/for}}
</script>    
<script id="message_list" type="text/x-jsrender">
      {{for list}}

				<tr class="messageRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:id}}</td>
                    <td>{{:sender_name}}</td>
                    <td>{{:receiver_name}}</td>
                    <td class="txt">{{:title}}</td>
                    <td class="reply_btn">
						{{if sender_type == 'U'}}
							<button class="blue_btn center answer-btn" data-id="{{:sender_id}}">답장 하기</button>
						{{/if}}
					</td>
                    <td>{{:create_date}}</td>
                </tr>


     {{/for}}
</script>    
<script id="admin_list" type="text/x-jsrender">
      {{for list}}
				<tr  class="adminRead" data-id="{{:id}}">
                    <td>{{:rownum}}</td>
                    <td>{{:admin_id}}</td>
                    <td>{{:name}}</td>
                    <td>{{:create_date}}</td>
 					<td class="delete_btn">
                        <input type="button" value="수정" onclick="location.href='/supervise/setting/manager/update/{{:id}}'"/> / 
                        <input type="button" value="삭제" class="delete-btn"/>
                    </td>
                </tr>
     {{/for}}
</script>    
<script id="message_user" type="text/x-jsrender">
      {{for data}}
				 <tr>
                 	<td class="number"><input type="checkbox" class="chk" value="{{:id}}"></td>
                 	<td class="number">{{:id}}</td>
                    <td>{{:email}}</td>
                 </tr>
     {{/for}}
</script>    
<script id="spot_option" type="text/x-jsrender">
	  <option value="">선택해주세요.</option>
      {{for data}}
				<option value="{{:id}}">{{:name}}</option>
      {{/for}}
</script>    
<script id="info_workman_list" type="text/x-jsrender">
	{{for data}}
		<tr class="tr-radio">
	                        <td class="number">
	                            <div class="checks">
	                                  <input type="radio" id="check{{:#index}}" name="user_id" value="{{:id}}"> 
	                                  <label for="check{{:#index}}"></label> 
	                            </div>
	                        </td>
	                        <td>{{:name}}</td>
	                        <td>{{hipen:phone}}</td>
	                    </tr>
	{{/for}}
</script>    
<script id="info_shop_list" type="text/x-jsrender">
	{{for list}}
		<tr class="tr-radio">
	                        <td class="number">
	                            <div class="checks">
	                                  <input type="radio" id="check{{:#index}}" name="shop_id" value="{{:id}}" data-user-id="{{:user_id}}"> 
	                                  <label for="check{{:#index}}"></label> 
	                            </div>
	                        </td>
	                        <td>{{:name}}</td>
	                        <td>{{hipen:tel}}</td>
	                    </tr>
	{{/for}}
</script>    