<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <style>
        body{background: pink}
        .img_pop{width: 600px; background: #fff; border-radius: 10px; font-size: 16px; text-align: center; padding: 30px;}
        .img_area{position: relative; width: 486px; height: 500px; background: #eee; margin: 0 auto;}
        .img_area > span{position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);}
        .upload_btn{margin: 30px 0 5px 0;}
        .upload_btn:after{content: ""; display: block; clear: both;}
        .upload_btn > div{display: inline-block; height: 40px; width: 150px; margin: 0 10px;}
        #target_img{width: 100%;height: 100%;}
        
        /* 첨부 파일 */
        .upload_btn > .upload_file {position: relative; margin-top: 1px;}
        .upload_file > [type='file'] {position: absolute; top: 0; left: 0; width: 100%; height: 100%; opacity: 0; z-index: 10; cursor: pointer;}
        .upload_file > .button {display: inline-block; cursor: pointer; background: #4753fe linear-gradient( to bottom, #7079ff, #4753fe ); color: #fff; height: 40px; width: 100%; text-align: center; line-height: 40px;}
        .save_btn{display: inline-block; cursor: pointer; background: #4753fe linear-gradient( to bottom, #7079ff, #4753fe ); border: 0; color: #fff; height: 40px; width: 100%; text-align: center; line-height: 40px; font-size: 16px;}
			
    </style>
    
    <script>
    	$(function(){
    		var table_name = "${param.table_name}";
    		var column_name = "${param.column_name}";
    		var id = "${param.id}";
    		
    		var param = {
    				"table_name":table_name,
   					"column_name":column_name,
   					"id":id
    		}
    		ajaxCallPost("/api/v1/common/file/get", param, function(res){
				if(res.success){
					$("#target_img").attr("src", res.data.file_url);
		    		$("input[name=file_url]").val(res.data.file_url);
				}
			})
    		
    		
    		$(".save_btn").click(function(){
    			
    			var file_url = $("input[name='file_url']");
    			if(!file_url.val()){
    				alert("파일을 등록해주세요.")
    				return;
    			}
    			
    			var param = {
   					"table_name":table_name,
   					"column_name":column_name,
   					"id":id,
   					"file_url":file_url.val()
    			}
    			ajaxCallPost("/api/v1/common/file/save", param, function(res){
    				if(res.success){
    					alert("등록이 완료되었습니다.");
    					self.close();
    				}else{
    					alert("등록에 실패하였습니다")
    				}
    			})
    		})
    		
    		$("input[name='file']").change(previewImgCreateFront2);
    	})
    </script>
    
    <div class="img_pop">
        <div class="img_area">
        	<img src="" id="target_img">
        </div>
        <div class="upload_btn">
            <div class='upload_file'>
              <input type='file' name="file">
              <input type="hidden" name="file_url">
              <span class='button'>파일 찾기</span>
            </div>
            <div><button class="save_btn">저장</button></div>
        </div>
    </div>
