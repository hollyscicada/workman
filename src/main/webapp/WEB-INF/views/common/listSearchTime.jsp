<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
	$(function(){
		
		/*일자 검색 돋보기 클릭시*/
		$(".content1 .search-time").click(function(){
			globalpage1 = 1;
			init1(globalpage1);
		})
		$(".content2 .search-time").click(function(){
			globalpage2 = 1;
			init2(globalpage2);
		})
		
	})
	function searchToday(){
	    var now = new Date();
	    var year = now.getFullYear();
	    var month = now.getMonth() + 1;    //1월이 0으로 되기때문에 +1을 함.
	    var date = now.getDate();
	
	    month = month >=10 ? month : "0" + month;
	    date  = date  >= 10 ? date : "0" + date;
	    return ""+year + "-" + month + "-" + date; 
	}
</script>
