<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
	var globalSortValue = "asc";
	$(function(){
		
		/*정렬 타입 변경시*/
		$(".order_search").change(function(){
			globalpage = 1;
			init(globalpage);
		})
		
		/*정렬 타입 오름차순*/
		$(".order_up").click(function(){
			globalSortValue = "desc";
			globalpage = 1;
			init(globalpage);
		})
		
		/*정렬 타입 내림차순*/
		$(".order_down").click(function(){
			globalSortValue = "asc";
			globalpage = 1;
			init(globalpage);
		})
		
		/*검색 돋보기 클릭시*/
		$(".search_btn").click(function(){
			globalpage = 1;
			init(globalpage);
		})
		
		/*검색 타입 변경시*/
		$("#search_type").change(function(){
			$("#keyword").focus();
		})
		
		/*검색 키워드 엔터시*/
		$("#keyword").keydown(function(key) {
            if (key.keyCode == 13) {
            	$(".search_btn").trigger("click");
            }
        });
	})
</script>