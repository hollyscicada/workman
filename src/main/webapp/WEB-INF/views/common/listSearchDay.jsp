<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
	.search-day-active{
		background: #4753fe linear-gradient( to bottom, #7079ff, #4753fe ) !important;
    	border: 1px solid #4753fe !important;
	}
</style>
<script>
	$(function(){
		
		/*장기 비로그인*/
    	$(document).on("click", ".long-tile-loing-btn", function(e){
    		
    		if($(this).prop("checked")){
    			$(".search-day-btn").removeClass("search-day-active");
       			
       			var month = searchMonth();
       			$("input[name=start_dt]").val("2020-01-01");
       			$("input[name=end_dt]").val(month);
       			$("input[name=start_dt]").attr("disabled", true);
       			$("input[name=end_dt]").attr("disabled", true);
    		}else{
    			$("input[name=start_dt]").attr("disabled", false);
       			$("input[name=end_dt]").attr("disabled", false);
    		}
    	})

		
		/*날짜 당일 클릭시*/
		$("#search-today").click(function(){
			$(".search-day-btn").removeClass("search-day-active");
			
			$("input[name=start_dt]").attr("disabled", false);
   			$("input[name=end_dt]").attr("disabled", false);
			$(".long-tile-loing-btn").attr("checked", false);
			
			$(this).addClass("search-day-active")
			
			var today = searchToday();
			$("input[name=start_dt]").val(today);
			$("input[name=end_dt]").val(today);
		})
		
		/*날짜 1주 클릭시*/
		$("#search-week").click(function(){
			$(".search-day-btn").removeClass("search-day-active")
			
			$("input[name=start_dt]").attr("disabled", false);
   			$("input[name=end_dt]").attr("disabled", false);
			$(".long-tile-loing-btn").attr("checked", false);
			
			$(this).addClass("search-day-active")
			
			var week = searchWeek();
			var today = searchToday();
			$("input[name=start_dt]").val(week);
			$("input[name=end_dt]").val(today);
		})
		
		/*날짜 1달 클릭시*/
		$("#search-month").click(function(){
			$(".search-day-btn").removeClass("search-day-active")
			
			$("input[name=start_dt]").attr("disabled", false);
   			$("input[name=end_dt]").attr("disabled", false);
			$(".long-tile-loing-btn").attr("checked", false);
			
			$(this).addClass("search-day-active")
			
			var month = searchMonth();
			var today = searchToday();
			$("input[name=start_dt]").val(month);
			$("input[name=end_dt]").val(today);
		})
		
		
		/*일자 검색 돋보기 클릭시*/
		$("#search-date").click(function(){
			globalpage = 1;
			init(globalpage);
		})
		
		$("input[name=start_dt], input[name=end_dt]").change(function(){
			$(".search-day-btn").removeClass("search-day-active")
		})
		
	})
	function searchToday(){
	    var now = new Date();
	    var year = now.getFullYear();
	    var month = now.getMonth() + 1;    //1월이 0으로 되기때문에 +1을 함.
	    var date = now.getDate();
	
	    month = month >=10 ? month : "0" + month;
	    date  = date  >= 10 ? date : "0" + date;
	    return ""+year + "-" + month + "-" + date; 
	}
	function searchWeek() {
		var now = new Date();
	    var year = now.getFullYear();
	    var month = now.getMonth() + 1;    //1월이 0으로 되기때문에 +1을 함.

	    var date = now.getDate();
	    now.setDate(date-7);
	    date = now.getDate();
	
	    month = month >=10 ? month : "0" + month;
	    date  = date  >= 10 ? date : "0" + date;
	    return ""+year + "-" + month + "-" + date; 
	}
	
	function searchMonth() {
		var now = new Date();
	    var year = now.getFullYear();
	    
	    var month = now.getMonth() + 1;    //1월이 0으로 되기때문에 +1을 함.
	    now.setMonth(month-1);
	    month = now.getMonth();

	    var date = now.getDate();
	
	    month = month >=10 ? month : "0" + month;
	    date  = date  >= 10 ? date : "0" + date;
	    return ""+year + "-" + month + "-" + date; 
	}
</script>
<div class="term_search">
    <span>기간 검색</span>
    <input type="date" name="start_dt">&nbsp;~&nbsp;
    <input type="date" name="end_dt">
    <div>
        <input type="button" name="" id="search-today"  value="당일" class="opt_btn search-day-btn" />
        <input type="button" name="" id="search-week"  value="1주" class="opt_btn search-day-btn" />
        <input type="button" name="" id="search-month"  value="1달" class="opt_btn search-day-btn" />
        <input type="button" name="" id="search-date" value="검색" class="blue_btn">
    </div>
    
</div>