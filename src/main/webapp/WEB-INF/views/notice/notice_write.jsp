<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/notice_write.css" />
    <script>
    	$(function(){
    		$(".submit-btn").click(function(){
    			
    			var title = $("input[name=title]");
    			var content = $("textarea[name=content]");
    			var file_url = $("input[name=file_url]");
    			
    			if(!title.val()){
    				alert("제목을 입력해주세요.");
    				title.focus();
    				return;
    			}
    			if(!content.val()){
    				alert("내용을 입력해주세요.");
    				content.focus();
    				return;
    			}
    			
    			var param = {
    					"title":title.val(),
    					"content":content.val(),
    					"file_url":file_url.val(),
    					"admin_id":"${sessionScope.MEMBER.id }"
    			}
    			ajaxCallPost("/api/v1/notice/create", param, function(res){
    				if(res.success){
    					alert("해당 데이터를 추가하였습니다.");
    					location.href="/supervise/notice"
    				}else{
    					alert("등록에 실패하였습니다.")
    				}
    			})
    		})
    		
    		$("input[name='file']").change(previewImgCreateFront);
    	})
    </script>
        <div class="content">            
            <ul class="write_area">
                <li>
                    <p>제목</p>
                    <input type="text" name="title" />
                </li>
                <li>
                    <p>내용</p>
                    <textarea name="content"></textarea>

                </li>
                <li>
                    <p>첨부파일</p>
                    <div class='file-input'>
                      <input type='file' name="file">
                      <input type="hidden" name="file_url">
                      <span class='button'>파일 찾기</span>
                      <span class='label file_url_text' data-js-label>선택된 파일이 없습니다.</span>
                    </div>
                </li>
            </ul>
            <div class="submit_btn">
                <input type="button" value="목록보기" onclick="javascript:location.hef='/supervise/notice'" />
                <input type="submit" value="공지사항 등록" class="blue_btn submit-btn"/>
            </div>
        </div>
