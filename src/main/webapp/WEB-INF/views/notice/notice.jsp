<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/notice.css" />
    
<script>
    var globalpage = 1;
    $(function(){
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    })
    function init(page){
        globalpage = page;
        var param = {
       		"page":page,
       		"search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val()
        }
        ajaxCallPost("/api/v1/notice/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#notice_list").render(res.data));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
            }
        })
    }
    
    $(function(){
    	
       	$(document).on("dblclick", ".noticeRead", function(e){
       		var id = $(this).attr("data-id");
       		var param = {
       			"id":id
       		}
       		
       		ajaxCallPost("/api/v1/notice/read", param, function(res){
       			if(res.success){
       				var item = res.data.read;	
          				$("#p_title").val(item.title);
          				$("#p_admin_name").val(item.admin_name);
          				$("#p_create_date").val(item.create_date);
          				$("#p_content").val(item.content);
          				$("#p_file_url").attr("data-id", item.id);
          				
       				$('.pop_up').css({"display" : "block"});
       			}
       		})
       	})
        	
    	
    	$(document).on("click", ".update-btn", function(){
    		var id = $(this).parents("tr").attr("data-id");
    		location.href="/supervise/notice/update/"+id;
    	})
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).parents("tr").attr("data-id");
	    		var param = {
	    				"table_name":"notice",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    })
</script>
    <div class="content">
        <div class="search_area">
            <div class="new_notice">
                <a href="/supervise/notice/write" class="blue_btn">공지사항 등록</a>
            </div>
            <div class="search">
                <span>검색</span>
                <select id="search_type" name="sort">
                    <option value="">전체</option>
                    <option value="notice_id">번호</option>
                    <option value="admin_name">작성자</option>
                </select>
                <div class="search_txt">
                    <input type="text" name="keyword" id="keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>

        </div>
        <div class="table_wrap">   
        <table>
            <thead>
                <tr class="table_more">
                    <th class="number">NO.</th>
                    <th>제목</th>
                    <th>작성자</th>
                    <th>작성일</th>
                    <th>수정 / 삭제</th>
                </tr>
            </thead>

            <tbody class="tbody">
            	<%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>

        </table>                
        </div>  
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up notice_pop">
          <div class="pop_inner center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
           
         
               <ul class="pop_content">
         
                    <li><span>작성자</span><input type="text" id="p_admin_name" value="" disabled="disabled"></li>
                    <li><span>작성일</span><input type="text" id="p_create_date" value="" disabled="disabled" /></li>   
                    <li><span>제목</span><input type="text" id="p_title" value="" disabled="disabled"></li>
                    <li><span>내용</span><textarea id="p_content"  disabled="disabled"></textarea></li> 
                
               </ul>
               <div class="button">
                 <button type="button" class="file-pop-btn" id="p_file_url" data-id="" data-column-name="file_url" data-table-name="notice">첨부파일</button>
              </div>
           </div>
       </div>
