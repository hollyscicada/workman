<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page_num main-page-div">
	<span class="pageAll">
	</span>
    <div class="arrow">
        <a class="left_arrow prev gopage"><img src="/resources/images/left_arrow.png" /></a>
        <a class="right_arrow next gopage"><img src="/resources/images/right_arow.png" /></a>
    </div>
</div>