<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
 <link rel="stylesheet" href="/resources/css/login.css" />
<script>
		$(function(){
			
			
			$("#admin_id").keydown(function(key) {
                if (key.keyCode == 13) {
                	$("#password").focus();
                }
            });
            $("#password").keydown(function(key) {
                if (key.keyCode == 13) {
                	$(".loginbtn").trigger("click");
                }
            });
			
			$(".loginbtn").click(function(){
				var admin_id = $("#admin_id").val();
				var pw = $("#password").val();
				
				var param = {
						"admin_id" : admin_id,
						"pw" : pw
				}
				
				ajaxCallPost("/api/v1/login", param, function(res){
					if(res.success){
						location.href="/supervise";
					}else{
						alert("아이디 또는 비밀번호를 확인해주세요.")
					}
				},function(){})
			})
			$("#admin_id").focus();
		})
	</script>
    
    
       <div class="login">
           <img src="/resources/images/logo.png" class="logo"/>
           <p>관리자 로그인</p>
           
           <label for="password">아이디</label>
           <input type="text" name="admin_id" id="admin_id" placeholder="아이디를 입력하세요."/>
           <label for="password">비밀번호</label>
           <input  type="password" name="pw" id="password" placeholder="비밀번호를 입력하세요."/>
           <input type="submit" class="loginbtn" value="로그인"/>
           
       </div>
