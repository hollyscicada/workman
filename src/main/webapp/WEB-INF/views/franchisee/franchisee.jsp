<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/franchisee.css" />
<script>
	var time = 5000;
	var poling;
    var globalpage = 1;
    $(function () {
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    });
    function startPoling(){
        poling = setInterval(function(){
            if(globalpage == 1){
                init(globalpage);
             }
        }, time);
    }
    function endPoling(){
        clearInterval(poling)
    }
    function init(page){
        globalpage = page;
        endPoling();
        startPoling();
         var param = {
            "page":page,
            "search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val(),
       		"sort_type":$("#sort_type").val(),
       		"sort_value":globalSortValue
        }
        ajaxCallPost("/api/v1/franchisee/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#franchisee_list").render(res.data));


                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){
                	if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
				$(".total-franchisee-cnt").text(res.data.listSize)


            }
        })
    }
    
    
    
    $(function(){
    	$(document).on("dblclick", ".franchiseeRead", function(e){
    		var id = $(this).attr("data-id");
    		
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/franchisee/read", param, function(res){
    			if(res.success){
    				var item = res.data.read;	
       				$("#p2_id").val(item.id);
       				$("#p2_name").val(item.name);
       				$("#p2_email").val(item.email);
       				$("#p2_job_json").val(global.job_format_multi(item.job_json));
       				$("#p2_tel").val(global.hipen(item.tel));
       				$("#p2_phone").val(global.hipen(item.phone));
       				$("#p2_location").val(item.location);
       				$("#p2_sub_location").val(item.sub_location);
       				$("#p2_user_name").val(item.user_name);
       				$("#p2_register_count").val(item.register_count);
       				$("#p2_accept_count").val(item.accept_count);
       				$("#p2_cancel_count").val(item.cancel_count);
       				$("#p2_prepaid_flag").val(item.prepaid_flag ? 1 : 0);
       				$("#p2_sex").val(item.sex);
       				$("#p2_country").val(item.country);
       				$("#p2_cash").val(item.cash);
       				$("#p2_branch_name").val(item.branch_name);
       				$("#p2_branch_tel").val(item.branch_tel);
       				
       				var status_flag_text = "";
       				if(item.status_flag == 0){
       					status_flag_text = "미승인"
       				}else{
       					status_flag_text = "승인"
       				}
       				$(".status_flag_text").text(status_flag_text);
       				
       				$("#p2_business").attr("data-id", item.user_id);
       				$("#p2_permit").attr("data-id", item.user_id);
       				$(".modify-btn").attr("data-user-id", item.user_id);
       				$(".modify-btn").attr("data-id", item.id);
       				$(".delete-btn").attr("data-id", item.id);
       				
       				
       				var param = {
       			            "page":"1"
       			        }
      			        ajaxCallPost("/api/v1/spot/list", param, function(res){
      			            if(res.success){
      			            	$("#p2_branch_id").html($("#spot_option").render(res));
      			            	$("#p2_branch_id").val(item.branch_id);
      			            	$('.pop_up').css({"display" : "block"});
      			            }
      			        })
    			}
    		})
    	})
    	$(document).on("click", ".status-update-btn", function(){
    		var id = $("#p2_id").val();
    		var status_flag = $(this).attr("data-status-flag");
    		
    		
    		var param = {
    				"id":id,
    				"status_flag":status_flag
    		}
    		ajaxCallPost("/api/v1/franchisee/join/status", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
  				    $('.pop_inner').show();
        			$('.join_pop').hide();
    				$( '.close_btn' ).trigger("click");
    				init(globalpage);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	
    	
    	$(document).on("click", ".modify-btn", function(){
    		var id = $(this).attr("data-id");
    		var user_id = $(this).attr("data-user-id");
    		var name = $("#p2_name");
    		var email = $("#p2_email");
    		var tel = $("#p2_tel");
    		var phone = $("#p2_phone");
    		var location = $("#p2_location");
    		var sub_location = $("#p2_sub_location");
    		var user_name = $("#p2_user_name");
    		var register_count = $("#p2_register_count");
    		var accept_count = $("#p2_accept_count");
    		var cancel_count = $("#p2_cancel_count");
    		var prepaid_flag = $("#p2_prepaid_flag");
    		var sex = $("#p2_sex");
    		var country = $("#p2_country");
    		var cash = $("#p2_cash");
    		var branch_id = $("#p2_branch_id");
    		
    		
    		if(!branch_id.val()){
    			alert("관리지점은 필수입니다.");
    			return;
    		}
    		
    		var param = {
    				"id":id,
    				"name":name.val(),
    				"email":email.val(),
    				"tel":tel.val(),
    				"phone":phone.val(),
    				"location":location.val(),
    				"sub_location":sub_location.val(),
    				"user_name":user_name.val(),
    				"register_count":register_count.val(),
    				"accept_count":accept_count.val(),
    				"cancel_count":cancel_count.val(),
    				"prepaid_flag":prepaid_flag.val(),
    				"sex":sex.val(),
    				"country":country.val(),
    				"cash":cash.val(),
    				"branch_id":branch_id.val(),
    				"user_id":user_id
    		}
    		ajaxCallPost("/api/v1/franchisee/update", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
    				$( '.close_btn' ).trigger("click");
    				init(globalpage);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).attr("data-id");
	    		var param = {
	    				"table_name":"shop",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/franchisee/remove", param, function(res){
	    			if(res.success){
	    				
	    				
	    				ajaxCallPost("/api/v1/common/remove", param, function(res){
	    	    			if(res.success){
	    	    				alert("해당 데이터를 삭제하였습니다.")
	    	    				$( '.close_btn' ).trigger("click");
	    	    				init(globalpage);
	    	    			}else{
	    	    				alert("삭제에 실패하였습니다.")
	    	    			}
	    	    		})
	    				
	    			
	    				
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
				})
	    		
    		}
    		
    	})
    })

</script>



    
    <div class="content">
        <div class="franchisee_count">현재 가맹점수 <span class="total-franchisee-cnt"></span></div>
            <div class="search_area">
                <div class="search">
                    <span>검색</span>
                    <select id="search_type" name="sort">
                        <option value="">전체</option>
                        <option value="shop_id">가맹점 번호</option>
                        <option value="show_name">상호명</option>
                        <option value="shop_tel">전화번호</option>
                        <option value="branch_name">관리지점</option>
                        <option value="1">업종</option>
                        <option value="user_phone">핸드폰 번호</option>
                    </select>
                    <div class="search_txt">
                        <input type="text" name="keyword" id="keyword" value="" />
                        <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                    </div>
                </div>  
                <div class="order_search">
                    <span>정렬</span>
                    <select id="sort_type" name="" class="option">
                        <option value="shop_id">전체</option>
                        <option value="2">회원비 납부여부</option>
                        <option value="register_count">배정요청 건수</option>
                        <option value="accept_count">배정완료 건수</option>
                        <option value="cancel_count">취소 건수</option>
                        <option value="cash">캐쉬 잔액</option>
                    </select>
                    <div class="order_btn">
                        <button class="order_up"><img src="/resources/images/up_off.png" alt="오름차순"></button>
                        <button class="order_down"><img src="/resources/images/down_on.png" alt="내림차순"></button>
                    </div>
                </div> 
        </div>  
		<div class="table_wrap">
			<table>
				<thead>
					<tr class="table_more">
						<th class="number">NO.</th>
						<th>가맹점 번호</th>
						<th>상호명</th>
						<th>email</th>
						<th>대표자명</th>
						<th>업종</th>
						<th>승인여부</th>
						<th>전화번호</th>
						<th>핸드폰 번호</th>
						<th>주소</th>
						<th>배정요청<br>건수</th>
						<th>배정완료<br>건수</th>
						<th>취소 건수</th>
						<th>회원비<br>납부여부</th>
						<th>결제방식</th>
						<th>캐쉬 잔액</th>
						<th>관리 지점명</th>
						<th>관리지점<br>전화번호</th>
					</tr>
				</thead>

				<tbody class="tbody">
				<%@ include file="/WEB-INF/views/common/default.jsp"%>
				</tbody>

			</table>   
		</div>               
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up">
          <div class="pop_inner franchisee_pop center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div class="top">
                    <div><span>가맹점 번호</span><input type="text" id="p2_id" value="" disabled="disabled"></div>
                    <div><span>상호명</span><input type="text" id="p2_name" value=""></div>
                    <div><span>회원가입 승인여부</span><span class="submit_result status_flag_text">승인</span></div>
                    <input type="button" value="회원가입 승인" class="blue_btn join_submit"/>
               </div>
               <ul class="pop_content">
                   <li class="section_1">
                        <div><span>email</span><input type="text" id="p2_email" value=""  disabled="disabled"/></div>   
                        <div><span>업종</span><input type="text" id="p2_job_json" value="" disabled="disabled"/></div>   
                        <div><span>전화번호</span><input type="text" id="p2_tel" value="" /></div>   
                        <div><span>핸드폰 번호</span><input type="text" id="p2_phone" value="" /></div>   
                        <div><span>주소</span><input type="text" id="p2_location" value="" /></div>   
                        <div><span>상세주소</span><input type="text" id="p2_sub_location" value="" /></div>   
                   </li>
                   <li class="section_2">
                        <div><span>이름</span><input type="text" id="p2_user_name" value="" /></div>   
                        <div><span>배송요청 건수</span><input type="text" id="p2_register_count" value="" /></div>   
                        <div><span>배송완료 건수</span><input type="text" id="p2_accept_count" value="" /></div>   
                        <div><span>취소 건수</span><input type="text" id="p2_cancel_count" value="" /></div>   
                        <div><span>회원비<br>납부여부</span>
                        	<select id="p2_prepaid_flag">
                        		<option value="">선택해주세요.</option>
                        		<option value="1">납부완료</option>
                        		<option value="0">납부안됨</option>
                        	</select>
                        </div>   
                   </li>
                   <li class="section_3">
                        <div>
                        	<span>성별</span>
                        	<select id="p2_sex">
                        		<option value="">선택해주세요.</option>
                        		<option value="M">남자</option>
                        		<option value="W">여자</option>
                        	</select>
                        	
                        </div>   
                        <div><span>국적</span>
                        	<select id="p2_country">
                        		<option value="">선택해주세요.</option>
                        		<option value="내국인">내국인</option>
                        		<option value="외국인">외국인</option>
                        	</select>
                        </div>   
                        <div><span>결제방식</span><input type="text" id="" value="" disabled="disabled"/></div>   
                        <div><span>캐쉬잔액</span><input type="text" id="p2_cash" value="" /></div>   
                        <div><span>관리 지점명</span>
                        	<select id="p2_branch_id"></select>
                        </div>   
                        <div><span>관리지점<br>전화번호</span><input type="text" id="p2_branch_tel" value="" disabled="disabled"/></div>   
                   </li>
               </ul>
              <div class="button">
                 <button type="button" class="modify-btn" data-id="">수정</button>
                 <button type="button" class="delete-btn" data-id="">삭제</button>
                 <button type="button" class="file-pop-btn" id="p2_business" data-id="" data-column-name="business" data-table-name="user">사업자 등록증</button>
                 <button type="button" class="file-pop-btn" id="p2_permit" data-id="" data-column-name="permit" data-table-name="user">허가증</button>
              </div>
           </div>
        <!-- join submit pop-->
        <div class="join_pop center">
           <a href="#" class="inn_pop_close"><img src="/resources/images/close_btn.png" class="center"/></a>
            <p>회원 가입을 승인하시겠습니까?</p>
            <div>
                <input type="submit" value="승인" class="blue_btn status-update-btn" data-status-flag="1"/>
                    <input type="button" value="비승인" class="status-update-btn" data-status-flag="0"/>
            </div>
        </div>   
       </div>
