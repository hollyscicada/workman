<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/setting_manager.css" />
    <script>
    	$(function(){
    		$(".submit-btn").click(function(){
    			
    			var admin_id = $("input[name=admin_id]");
    			var pw = $("input[name=pw]");
    			var name = $("input[name=name]");
    			var admin_phone = $("input[name=admin_phone]");
    			
    			if(!admin_id.val()){
    				alert("ID를 입력해주세요.");
    				admin_id.focus();
    				return;
    			}
    			if(!pw.val()){
    				alert("Password을 입력해주세요.");
    				pw.focus();
    				return;
    			}
    			if(!name.val()){
    				alert("이름을 입력해주세요.");
    				name.focus();
    				return;
    			}
    			if(!admin_phone.val()){
    				alert("전화번호를 입력해주세요.");
    				admin_phone.focus();
    				return;
    			}
    			
    			var param = {
    					"admin_id":admin_id.val(),
    					"pw":pw.val(),
    					"name":name.val(),
    					"admin_phone":admin_phone.val()
    			}
    			ajaxCallPost("/api/v1/admin/create", param, function(res){
    				if(res.success){
    					alert("해당 데이터를 추가하였습니다.");
    					location.href="/supervise/setting/manager"
    				}else{
    					alert("등록에 실패하였습니다.")
    				}
    			})
    		})
    		
    	})
    </script>
    <div class="content manager_sub">
              <ul class="write_area">
                <li>
                    <p>ID</p>
                    <input type="text" name="admin_id" />
                </li>
                <li>
                    <p>Passwrod</p>
                    <input type="password" name="pw" />
                </li>
                <li>
                    <p>이름</p>
                    <input type="text" name="name" />
                </li>
                <li>
                    <p>전화번호</p>
                    <input type="number" name="admin_phone" />
                </li>
            </ul>
            <div class="submit_btn">
                <input type="button" value="목록보기" onclick="javascript:location.href='/supervise/setting/manager'" />
                <input type="submit" value="등록" class="blue_btn submit-btn"/>
            </div>
    </div>
