<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/setting_manager.css" />
    <style>
    	.temp-auth-chk-btn{background-color: grey !important; border: 1px solid grey !important;}
    	.auth-chk-btn{background-color: #4753fe !important; border: 1px solid #4753fe !important;}
    </style>
    <script>
    	var is_auth = false;
    	var auth_number = "";
    	$(function(){
    		$(".open-pw-btn").click(function(){
    			$(".pw_pop").show();
    		})
    		
    		$(document).on("input", "input[name=auth_number]", function(){
    			is_auth = false;
    			$(".temp-auth-chk-btn").addClass("auth-chk-btn")
    		})

    		$(document).on("click", ".auth-chk-btn", function(){
    			
    			var write_auth_number = $("input[name=auth_number]");
    			if(!write_auth_number.val()){
    				alert("인증번호를 입력해주세요.");
    				write_auth_number.focus();
    				return;
    			}
    			if(auth_number && auth_number == write_auth_number.val()){
    				alert("인증되었습니다.");
    				is_auth = true;
    				
    				$(".temp-auth-chk-btn").removeClass("auth-chk-btn")
    			}else{
    				alert("인증번호가 일치하지 않습니다.");
    				is_auth = false;
    				$(".temp-auth-chk-btn").addClass("auth-chk-btn")
    			}
    		})
    		$(".auth-number-btn").click(function(){
    			var phone = $("input[name=pw_phone]").val();
    			
    			if(!phone){
    				alert("휴대폰번호가 존재하지 않습니다.");
    				return;
    			}
    			var param = {
    					"phone":phone
    			}
    			ajaxCallPost("/api/v1/member/auth/rand/number", param, function(res){
    				if(res.success){
    					alert("인증번호를 전송하였습니다.");
    					auth_number = res.data.authNumber;
    				}else{
    					alert("인증번호에 실패하였습니다.")
    				}
    			})
    		})
    		$(".change-pw-btn").click(function(){
    			
    			if(!is_auth){
    				alert("휴대폰 인증을 해주세요.");
    				$("input[name=auth_number]").focus();
    				return;
    			}
    			
    			var pw = $("#pw");
    			var repw = $("#repw");
    			
    			if(!pw.val()){
    				alert("비밀번호를 입력해주세요.");
    				pw.focus();
    				return;
    			}
    			if(pw.val() != repw.val()){
    				alert("비밀번호와 확인 비밀번호가 일치하지 않습니다.");
    				repw.focus();
    				return;
    			}
    			
    			var param = {
    					"id":"${read.id}",
    					"pw":pw.val()
    			}
    			ajaxCallPost("/api/v1/admin/update/pw", param, function(res){
    				if(res.success){
    					alert("해당 데이터를 수정하였습니다.");
    					location.href="/supervise/setting/manager"
    				}else{
    					alert("수정에 실패하였습니다.")
    				}
    			})
    			
    		})
    		$(".modify-btn").click(function(){
    			var admin_id = $("input[name=admin_id]");
    			var name = $("input[name=name]");
    			var admin_phone = $("input[name=admin_phone]");
    			
    			if(!admin_id.val()){
    				alert("ID를 입력해주세요.");
    				admin_id.focus();
    				return;
    			}
    			if(!name.val()){
    				alert("이름을 입력해주세요.");
    				name.focus();
    				return;
    			}
    			if(!admin_phone.val()){
    				alert("전화번호를 입력해주세요.");
    				admin_phone.focus();
    				return;
    			}
    			
    			var param = {
    					"id":"${read.id}",
    					"admin_id":admin_id.val(),
    					"name":name.val(),
    					"admin_phone":admin_phone.val()
    			}
    			ajaxCallPost("/api/v1/admin/update", param, function(res){
    				if(res.success){
    					alert("해당 데이터를 수정하였습니다.");
    					location.href="/supervise/setting/manager"
    				}else{
    					alert("수정에 실패하였습니다.")
    				}
    			})
    		})
    	})
    </script>
    <div class="content manager_sub">
        <table>
            <tbody>
                <tr>
                    <td>아이디 *</td>
                    <td><input type="text" value="${read.admin_id }" name="admin_id" /></td>
                    <td>이름 *</td>
                    <td><input type="text" value="${read.name }" name="name" /></td>
                </tr>
                <tr>
                    <td>연락처 *</td>
                    <td class="tel"><input type="number" name="admin_phone" value="${read.admin_phone }" /></td>
                    <td>등록날짜</td>
                    <td>${read.create_date }</td>
                </tr>
            </tbody>
        </table>
        <div class="btn_area">
            <button class="open-pw-btn">비밀번호 변경</button>
            <button class="modify-btn">수정</button>
        </div>
    </div>
    
    
    
    
    
    <div class="content pw_pop" style="display: none">
      <div class="certification">
           <h3>휴대폰 인증</h3>
           <span>비밀번호 변경을 위한 본인 인증이 필요합니다.</span>
            <div class="sand_certification">  
               <input type="number" name="pw_phone" value="${read.admin_phone }" disabled="disabled"/>
               <input type="button" class="auth-number-btn" value="인증번호 전송" />
           </div>
           <div class="submit_certification">
              <div>
                   <input type="number" name="auth_number" placeholder="인증번호를 입력하세요" />
                   <img src="/resources/images/del_icon.png" alt="인증번호 지우기" class="del_icon"/>
              </div>
               <input type="button" class="temp-auth-chk-btn" value="인증" />
           </div>
        </div>
        <div class="change_pw">
            <p>변경 비밀번호</p>
            <input type="password" id="pw" />
            <p>변경 비밀번호 확인</p>
            <input type="password" id="repw" />
            <div class="button_area">
                <input type="button" value="취소" />
                <input type="submit" class="change-pw-btn" value="확인" />
            </div>
        </div>   
   </div>
