<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/setting_manager.css" />
<script>
    var globalpage = 1;
    $(function(){
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    })
    function init(page){
        globalpage = page;
        var param = {
       		"page":page
        }
        ajaxCallPost("/api/v1/admin/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#admin_list").render(res.data));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
            }
        })
    }
    
    $(function(){
    	
    	$(document).on("dblclick", ".adminRead", function(e){
       		var id = $(this).attr("data-id");
       		var param = {
       			"id":id
       		}
       		
       		ajaxCallPost("/api/v1/admin/read", param, function(res){
       			if(res.success){
       				var item = res.data.read;	
          				$("#p_admin_id").val(item.admin_id);
          				$("#p_name").val(item.name);
          				$("#p_create_date").val(item.create_date);
          				$("#p_admin_phone").val(global.hipen(item.admin_phone));
          				
       				$('.pop_up').css({"display" : "block"});
       			}
       		})
       	})
    	
    	
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).parents("tr").attr("data-id");
	    		var param = {
	    				"table_name":"admin",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    })
</script> 
    <div class="content">
        <%@ include file="/WEB-INF/views/setting/setting_header.jsp"%>
	      <script>
	      	$(function(){
	      		$(".manager").addClass("on");
	      	})
	      </script>
          
        <table>
            <thead>
                <tr>
                    <th class="number">번호</th>
                    <th>ID</th>
                    <th>이름</th>
                    <th>등록 날짜</th>
                    <th>수정 / 삭제</th>
                </tr>
            </thead>

            <tbody class="tbody">
            	<%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>
        </table>
        <button class="enrollment_btn" onclick="location.href='/supervise/setting/manager/create'">등록</button>        
    </div>
        <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
     
    
    <!-- pop_up -->
    <div class="pop_up">
          <div class="pop_inner center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div class="top">
                    <div><span>ID</span><input type="text" id="p_admin_id" value="" disabled="disabled"></div>
                    <div><span>이름</span><input type="text" id="p_name" value="" disabled="disabled"></div>
               </div>
               <ul class="pop_content">
                    <li class="section_1">
                        <div><span>등록 날짜</span><input type="text" id="p_create_date" value="" disabled="disabled" /></div>   
                        <div><span>전화번호</span><input type="text" id="p_admin_phone" value="" disabled="disabled" /></div>   
                   </li>
               </ul>
           </div>
       </div>
    
