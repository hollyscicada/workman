<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="tab_menu">
    <li data-tab="tab1" class="fees"><a href="/supervise/setting/fees">수수료율 설정</a></li>
    <li data-tab="tab2" class="punish"><a href="/supervise/setting/punish">지점 및 회원 징계</a></li>
    <li data-tab="tab3" class="manager"><a href="/supervise/setting/manager">관리자 관리</a></li>
</ul>