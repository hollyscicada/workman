<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/setting_punish.css" />
    <div class="content">
        <%@ include file="/WEB-INF/views/setting/setting_header.jsp"%>
      <script>
      	$(function(){
      		$(".punish").addClass("on");
      	})
      </script>
        <!--지점 및 회원 징계 -->
        <div class="content">
            <div class="punishment">
               <div class="spot">
                    <div class="punishment_search">
                           <div class="sub_title"><span></span>지점 징계</div>
                            <div class="search">
                                <span>검색</span>
                                <div class="search_txt">
                                    <input type="text" name="keyword" id="keyword" value="" placeholder="지점명을 입력하세요"/>
                                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                                </div>
                            </div>  
                        </div>     
                    <table>
                            <thead>
                                <tr>
                                    <th class="number">지점명</th>
                                    <th>징계 / 퇴출</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr id="" onclick="">
                                    <td class="number">지점명</td>
                                    <td class="punish_btn">
                                        <button class="blue_btn punish">징계</button>
                                        <button class="blue_btn out">퇴출</button>
                                    </td>
                                </tr>
                                <tr id="" onclick="">
                                    <td class="number">지점명</td>
                                    <td class="punish_btn">
                                        <button class="blue_btn punish">징계</button>
                                        <button class="blue_btn out">퇴출</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <!-- 페이지 번호 -->          
                    <div class="page_num">
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <div class="arrow">
                                <a href="#" class="left_arrow"><img src="/resources/images/left_arrow.png" /></a>
                                <a href="#" class="right_arrow"><img src="/resources/images/right_arow.png" /></a>
                            </div>
                        </div>
               </div>
               <div class="worker">
                    <div class="punishment_search">
                           <div class="sub_title"><span></span>일꾼 징계</div>
                            <div class="search">
                                <span>검색</span>
                                <div class="search_txt">
                                    <input type="text" name="keyword" id="keyword" value="" placeholder="일꾼명을 입력하세요"/>
                                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                                </div>
                            </div>  
                        </div>     
                    <table>
                        <thead>
                            <tr>
                                <th class="number">지점명</th>
                                <th>징계 / 퇴출</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr id="" onclick="">
                                <td class="number">지점명</td>
                                <td class="punish_btn">
                                    <button class="blue_btn punish">징계</button>
                                    <button class="blue_btn out">퇴출</button>
                                </td>
                            </tr>
                            <tr id="" onclick="">
                                <td class="number">지점명</td>
                                <td class="punish_btn">
                                    <button class="blue_btn punish">징계</button>
                                    <button class="blue_btn out">퇴출</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- 페이지 번호 -->          
                    <div class="page_num">
                        <a href="#" class="active">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <div class="arrow">
                            <a href="#" class="left_arrow"><img src="/resources/images/left_arrow.png" /></a>
                            <a href="#" class="right_arrow"><img src="/resources/images/right_arow.png" /></a>
                        </div>
                    </div>
               </div>
               <div class="franchisee">
                    <div class="punishment_search">
                           <div class="sub_title"><span></span>지점 징계</div>
                            <div class="search">
                                <span>검색</span>
                                <div class="search_txt">
                                    <input type="text" name="keyword" id="keyword" value="" placeholder="가맹점명을 입력하세요"/>
                                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                                </div>
                            </div>  
                        </div>     
                    <table>
                        <thead>
                            <tr>
                                <th class="number">지점명</th>
                                <th>징계 / 퇴출</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr id="" onclick="">
                                <td class="number">지점명</td>
                                <td class="punish_btn">
                                    <button class="blue_btn punish">징계</button>
                                    <button class="blue_btn out">퇴출</button>
                                </td>
                            </tr>
                            <tr id="" onclick="">
                                <td class="number">지점명</td>
                                <td class="punish_btn">
                                    <button class="blue_btn punish">징계</button>
                                    <button class="blue_btn out">퇴출</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- 페이지 번호 -->          
                    <div class="page_num">
                        <a href="#" class="active">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <div class="arrow">
                            <a href="#" class="left_arrow"><img src="/resources/images/left_arrow.png" /></a>
                            <a href="#" class="right_arrow"><img src="/resources/images/right_arow.png" /></a>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>    
    <div class="punish_pop_up">
        <div class="pop_back"></div>
        <div class="punish_pop center">
            <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
            <p><span>A 지점</span>을 몇일 징계하시겠습니까?</p>
            <div>
                <input type="button" value="1일" />
                <input type="button" value="7일" />
                <input type="button" value="30일" />
                <input type="button" value="평생" />
            </div>
            <input type="submit" value="확인"  class="submit_btn blue_btn"/>
        </div>
         <div class="out_pop center">
             <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
            <p><span>A 지점</span>을 퇴출하시겠습니까?</p>
            <div>
                <input type="submit" value="확인" class="blue_btn"/>
                <input type="button" value="취소" />
            </div>
        </div>
    </div>
