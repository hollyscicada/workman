<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/setting_fees.css" />
    <div class="content">
      <%@ include file="/WEB-INF/views/setting/setting_header.jsp"%>
      <script>
      	$(function(){
      		$(".fees").addClass("on");
      	})
      </script>
       <!-- 수수료율 설정 -->
        <div class="fees_content">
            <div class="shop_fees">
                <div class="sub_title"><span></span>지점 수수료 설정</div>
                <div class="search_area">
                    <div class="search">
                        <span>검색</span>
                        <div class="search_txt">
                            <input type="text"  placeholder="지점명을 입력하세요."/>
                            <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                        </div>
                    </div>   
                    <div class="change_fees">
                        <div class="fees_txt">
                            <input type="text"  placeholder="수정할 수수료를 입력하세요."/><span class="percent">%</span>
                            <button class="blue_btn input_btn">입력</button>
                        </div>
                    </div>
                </div>      
                <table>
                    <thead>
                        <tr>
                            <th class="number">
                                <div class="checks">
                                      <input type="checkbox" id="check"> 
                                      <label for="check"><span>선택</span></label> 
                                </div>
                            </th>
                            <th>지점명</th>
                            <th>현재 수수료율</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="number">
                                <div class="checks">
                                      <input type="checkbox" id="check1"> 
                                      <label for="check1"></label> 
                                </div>
                            </td>
                            <td>지점명</td>
                            <td>100 %</td>
                        </tr>
                    </tbody>
                </table>
                <!-- 페이지 번호 -->          
                <div class="page_num">
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <div class="arrow">
                        <a href="#" class="left_arrow"><img src="/resources/images/left_arrow.png" /></a>
                        <a href="#" class="right_arrow"><img src="/resources/images/right_arow.png" /></a>
                    </div>
                </div>
             </div>
             <div class="workman_fees">
                <div class="sub_title"><span></span>일꾼 월회비 설정</div>
                <div class="search_area">
                    <div class="search">
                        <span>검색</span>
                        <div class="search_txt">
                            <input type="text"  placeholder="일꾼명을 입력하세요."/>
                            <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                        </div>
                    </div>   
                    <div class="change_fees">
                        <div class="fees_txt">
                            <input type="text"  placeholder="수정할 월회비를 입력하세요."/><span class="percent">원</span>
                            <button class="blue_btn input_btn">입력</button>
                        </div>
                    </div>
                </div>      
                <table>
                    <thead>
                        <tr>
                            <th class="number">
                                <div class="checks">
                                      <input type="checkbox" id="check2"> 
                                      <label for="check2"><span>선택</span></label> 
                                </div>
                            </th>
                            <th>일꾼명</th>
                            <th>현재 월회비</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="number">
                                <div class="checks">
                                      <input type="checkbox" id="check3"> 
                                      <label for="check3"></label> 
                                </div>
                            </td>
                            <td>일꾼명</td>
                            <td>10,000원</td>
                        </tr>
                    </tbody>
                </table>
                <!-- 페이지 번호 -->          
                <div class="page_num">
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <div class="arrow">
                        <a href="#" class="left_arrow"><img src="/resources/images/left_arrow.png" /></a>
                        <a href="#" class="right_arrow"><img src="/resources/images/right_arow.png" /></a>
                    </div>
                </div>
             </div>
        </div>               
    </div>
