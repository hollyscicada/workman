<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/request.css" />  
    <link rel="stylesheet" href="/resources/css/request1.css" />  
<script>
	var time = 5000;
	var poling;
    var globalpage = 1;
    $(function(){
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    })
    
    function startPoling(){
        poling = setInterval(function(){
            if(globalpage == 1){
                init(globalpage);
             }
        }, time);
    }
    function endPoling(){
        clearInterval(poling)
    }
    
    function init(page){
        globalpage = page;
        endPoling();
        startPoling();
        var param = {
       		"page":page,
       		"register_flag":"1",
       		"search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val(),
       		"day_type":"request",
       		"start_dt":$("input[name=start_dt]").val(),
       		"end_dt":$("input[name=end_dt]").val()
       		
        }
        ajaxCallPost("/api/v1/request/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#request_list").render(res.data));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
                
                $(".total-request-cnt").text(res.data.listSize);
                $(".no-request-cnt").text(res.data.noRequestSize);
                $(".yes-request-cnt").text(res.data.yesRequestSize);
                
                
                
            }
        })
    }
    
    $(function(){
    	$(document).on("dblclick", ".requestRead", function(e){
    		var id = $(this).attr("data-id");
    		
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/request/read", param, function(res){
    			if(res.success){
    				
    				var item = res.data.read;
    				$("#p_id").val(item.id);
    				$("#p_shop_id").val(item.shop_id);
    				$("#p_shop_name").val(item.shop_name);
    				$("#p_job_category").val(global.job_format(item.job_category));
    				
    				var status_falg_text = "";
					if(item.status_flag == 0){
						status_falg_text = "대기중"
					}else{
						status_falg_text = "배정완료"
					}    				
    				$("#p_status_flag").val(status_falg_text);
    				$("#p_pay").val(global.comma(item.pay));
    				$("#p_shop_tel").val(global.hipen(item.shop_tel));
    				$("#p_apply_date").val(item.apply_date);
    				$("#p_accept_date").val(item.accept_date);
    				$("#p_end_date").val(item.end_date);
    				$("#p_user_name").val(item.user_name);
    				$("#p_user_phone").val(global.hipen(item.user_phone));
    				$("#p_location").val(item.location);
    				$("#p_branch_name").val(item.branch_name);
    				$("#p_branch_tel").val(global.hipen(item.branch_tel));
    				
    				$(".delete-btn").attr("data-id", item.id);
    				$(".shop_info").attr("data-id", item.shop_id);
    				
    				$('.pop_up').css({"display" : "block"});
    			}
    		})
    	})
    	
    	/*
    	$('.shop_info').click(function(){
    		
    		var shop_id = $(this).attr("data-id");
    		var param = {
        			"id":shop_id
        		}
       		ajaxCallPost("/api/v1/franchisee/read", param, function(res){
       			if(res.success){
       				
       				var item = res.data.read;	
       				$("#p2_id").val(item.id);
       				$("#p2_name").val(item.name);
       				$("#p2_email").val(item.email);
       				$("#p2_job_json").val(global.job_format_multi(item.job_json));
       				$("#p2_tel").val(item.tel);
       				$("#p2_phone").val(item.phone);
       				$("#p2_location").val(item.location);
       				$("#p2_user_name").val(item.user_name);
       				$("#p2_register_count").val(item.register_count);
       				$("#p2_accept_count").val(item.accept_count);
       				$("#p2_cancel_count").val(item.cancel_count);
       				$("#p2_prepaid_flag").val(item.prepaid_flag);
       				$("#p2_sex").val(item.sex);
       				$("#p2_country").val(item.country);
       				$("#p2_cash").val(item.cash);
       				$("#p2_branch_name").val(item.branch_name);
       				$("#p2_branch_tel").val(item.branch_tel);
       				
       				
       				$('.shop_info_pop').css({"display" : "flex"});
       		   	    $('.reauest_pop').css({"display" : "none"});
       			}
       		})
   	    });
    	*/
    	
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).attr("data-id");
	    		var param = {
	    				"table_name":"work",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				$( '.close_btn' ).trigger("click");
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    	
    })
</script>
    <div class="content">
        <ul class="request_count"> 
            <li>현재 요청 건수 <span class="total-request-cnt"></span></li>
            <li>미배정 건수 <span class="no-request-cnt"></span></li>
            <li>배정 건수 <span class="yes-request-cnt"></span></li>
        </ul>
        <div class="search_area">
            <div class="search">
                <span>검색</span>
                <select id="search_type" name="sort">
                    <option value="">전체</option>
                    <option value="work_id">접수번호</option>
                    <option value="shop_name">상호명</option>
                    <option value="shop_tel">전화번호</option>
                    <option value="branch_name">관리지점</option>
                    <option value="user_name">일꾼 이름</option>
                    <option value="user_phone">일꾼 전화번호</option>
                </select>
                <div class="search_txt">
                    <input type="text" name="keyword" id="keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>  
            
            <%@ include file="/WEB-INF/views/common/listSearchDay.jsp"%>
        </div>
        <div class="table_wrap">   
        <table>
            <thead>
                <tr class="table_more">
                    <th class="number">NO.</th>
                    <th>접수 번호</th>
                    <th>상호명</th>
                    <th>업종</th>
                    <th>상태</th>
                    <th>급료</th>
                    <th>전화번호</th>
                    <th>접수시간</th>
                    <th>경과시간</th>
                    <th>배정시간</th>
                    <th>완료시간</th>
                    <th>일꾼명</th>
                    <th>일꾼 전화번호</th>
                    <th>주소</th>
                    <th>관리 지점명</th>
                    <th>관리지점 전화번호</th>
                    <th>결제방식</th>
                </tr>
            </thead>
            <tbody class="tbody">
            <%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>

        </table>    
        </div>              
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up">
    
    
    
    		<!-- 요청정보 팝업 -->
           <div class="pop_inner reauest_pop center">
            <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
            <div class="left">
                <h2>상점정보</h2>
                <ul>
                    <li>
                        <p class="list_title">가맹점 번호</p>
                        <div><input type="text" value="" id="p_shop_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상호명</p>
                        <div><input type="text" value="" id="p_shop_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">업종</p>
                        <div><input type="text" value="" id="p_job_category" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">급료</p>
                        <div><input type="text" value="" id="p_pay" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">전화번호</p>
                        <div><input type="text" value="" id="p_shop_tel" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">주소</p>
                        <div><input type="text" value="" id="p_location" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점명</p>
                        <div><input type="text" value="" id="p_branch_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점<br>전화번호</p>
                        <div><input type="text" value="" id="p_branch_tel" disabled="disabled"></div>
                    </li>
                </ul>
            </div>
        
            <div class="right">
                <h2>일꾼정보</h2>
                <ul>
                    <li>
                        <p class="list_title">접수번호</p>
                        <div><input type="text" value="" id="p_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼명</p>
                        <div><input type="text" value="" id="p_user_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼전화번호</p>
                        <div><input type="text" value="" id="p_user_phone" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상태</p>
                        <div><input type="text" value="" id="p_status_flag" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">접수시간</p>
                        <div><input type="text" value="" id="p_apply_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">경과시간</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">배정시간</p>
                        <div><input type="text" value="" id="p_accept_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">완료시간</p>
                        <div><input type="text" value="" id="p_end_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">결제방식</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
    
                </ul>
            </div>
    
            <div class="button">
                <button type="button" class="delete-btn">삭제</button>
            </div>
        </div>
           
	</div>