<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/workman.css" />
<script>
	var time = 5000;
	var poling;
	var globalpage = 1;
    $(function () {
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    });
    function startPoling(){
        poling = setInterval(function(){
            if(globalpage == 1){
                init(globalpage);
             }
        }, time);
    }
    function endPoling(){
        clearInterval(poling)
    }
    function init(page){
        globalpage = page;
        endPoling();
        startPoling();
        var day_type = "user";
        if($(".long-tile-loing-btn").prop("checked")){
        	day_type = "login_date";
        }
        
        var param = {
            "page":page,
            "search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val(),
       		"sort_type":$("#sort_type").val(),
       		"sort_value":globalSortValue,
       		"day_type":day_type,
       		"type_flag":"0",
       		"start_dt":$("input[name=start_dt]").val(),
       		"end_dt":$("input[name=end_dt]").val()
        }
        ajaxCallPost("/api/v1/workman/list", param, function(res){
        	 if(res.success){
                $(".tbody").html($("#work_man").render(res));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
            }
        })
    }
    
    
    $(function(){
    	$(document).on("dblclick", ".workmanRead", function(e){
    		var id = $(this).attr("data-id");
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/workman/read", param, function(res){
    			if(res.success){
    				var item = res.data.read;	
       				$("#p_id").val(item.id);
       				$("#p_email").val(item.email);
       				$("#p_name").val(item.name);
       				$("#p_sex").val(item.sex);
       				$("#p_job_json").val(global.job_format_multi(item.job_json));
       				$("#p_phone").val(global.hipen(item.phone));
       				$("#p_user_location").val(item.user_location);
       				$("#p_country").val(item.country);
       				$("#p_register_count").val(item.register_count);
       				$("#p_accept_count").val(item.accept_count);
       				$("#p_cancel_count").val(item.cancel_count);
       				$("#p_absent_count").val(item.absent_count);
       				$("#p_long_time").val(item.long_time);
       				$("#p_job_long_ing").val(item.job_long_ing);
       				
       				var status_flag_text = "";
       				if(item.status_flag == 0){
       					status_flag_text = "미승인"
       				}else{
       					status_flag_text = "승인"
       				}
       				$(".status_flag_text").text(status_flag_text);
       				
       				$("#p_type_flag").val(item.type_flag ? 1 : 0);
       				$("#p_prepaid_flag").val(item.prepaid_flag ? 1 : 0);
       				$("#p_create_date").val(item.create_date);
       				
       				
       				$("#p2_license").attr("data-id", item.id);
       				$("#p2_health").attr("data-id", item.id);
       				$(".modify-btn").attr("data-id", item.id);
       				$(".delete-btn").attr("data-id", item.id);
       				
       				
       				var param = {
       			            "page":"1"
       			        }
      			        ajaxCallPost("/api/v1/spot/list", param, function(res){
      			            if(res.success){
      			            	$("#p_branch_id").html($("#spot_option").render(res));
      			            	$("#p_branch_id").val(item.branch_id);
      			            	$('.pop_up').css({"display" : "block"});
      			            }
      			        })
       				
       				
    			}
    		})
    	})
    	
    	$(document).on("click", ".status-update-btn", function(){
    		var id = $("#p_id").val();
    		var status_flag = $(this).attr("data-status-flag");
    		
    		
    		var param = {
    				"id":id,
    				"status_flag":status_flag
    		}
    		ajaxCallPost("/api/v1/workman/join/status", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
  				    $('.pop_inner').show();
        			$('.join_pop').hide();
    				$( '.close_btn' ).trigger("click");
    				init(globalpage);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	
    	
    	
    	$(document).on("click", ".modify-btn", function(){
    		var id = $(this).attr("data-id");
    		var name = $("#p_name");
    		var sex = $("#p_sex");
    		var phone = $("#p_phone");
    		var user_location = $("#p_user_location");
    		var long_time = $("#p_long_time");
    		var job_long_ing = $("#p_job_long_ing");
    		var country = $("#p_country");
    		var register_count = $("#p_register_count");
    		var accept_count = $("#p_accept_count");
    		var cancel_count = $("#p_cancel_count");
    		var absent_count = $("#p_absent_count");
    		var type_flag = $("#p_type_flag");
    		var prepaid_flag = $("#p_prepaid_flag");
    		var branch_id = $("#p_branch_id");
    		
    		var param = {
    				"id":id,
    				"name":name.val(),
    				"sex":sex.val(),
    				"phone":phone.val(),
    				"user_location":user_location.val(),
    				"long_time":long_time.val(),
    				"job_long_ing":job_long_ing.val(),
    				"country":country.val(),
    				"register_count":register_count.val(),
    				"accept_count":accept_count.val(),
    				"cancel_count":cancel_count.val(),
    				"absent_count":absent_count.val(),
    				"type_flag":type_flag.val(),
    				"prepaid_flag":prepaid_flag.val(),
    				"branch_id":branch_id.val()
    		}
    		ajaxCallPost("/api/v1/workman/update", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
    				$( '.close_btn' ).trigger("click");
    				init(globalpage);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).attr("data-id");
	    		var param = {
	    				"table_name":"user",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				$('.pop_inner').show();
        				$('.join_pop').hide();
	    				$( '.close_btn' ).trigger("click");
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    })


</script>

    <div class="content">
        <div class="top_more_btn">
            <input type="checkbox" class="long-tile-loing-btn">장기 비로그인 계정보기
            <button class="blue_btn long_term">장기일당 계약자 등록</button>
        </div>
        <div class="search_area">
            <div class="search">
                <span>검색</span>
                <select id="search_type" name="sort">
                    <option value="">전체</option>
                    <option value="user_id">일꾼 번호</option>
                    <option value="user_name">일꾼 이름</option>
                    <option value="branch_name">관리지점</option>
                    <option value="">업종</option>
                    <option value="user_phone">핸드폰 번호</option>
                </select>
                <div class="search_txt">
                    <input type="text" name="keyword" id="keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>
            <div class="order_search">
                <span>정렬</span>
                <select id="sort_type" name="" class="option">
                    <option value="user_id">전체</option>
                    <option value="2">회원비 납부여부</option>
                    <option value="apply_count">지원 건수</option>
                    <option value="accept_count">배정 건수</option>
                    <option value="cancel_count">취소 건수</option>
                    <option value="absent_count">결근 횟수</option>
                </select>
                <div class="order_btn">
                    <button class="order_up"><img src="/resources/images/up_off.png" alt="오름차순"></button>
                    <button class="order_down"><img src="/resources/images/down_on.png" alt="내림차순"></button>
                </div>
            </div>   
            <%@ include file="/WEB-INF/views/common/listSearchDay.jsp"%>
        </div>
        <div class="table_wrap">   
        <table>
            <thead>
                <tr class="table_more">
                    <th class="number">NO.</th>
                    <th>일꾼 번호</th>
                    <th>email</th>
                    <th>이름</th>
                    <th>업종</th>
                    <th>승인여부</th>
                    <th>핸드폰 번호</th>
                    <th>주소</th>
                    <th>지원 건수</th>
                    <th>배정 건수</th>
                    <th>취소 건수</th>
                    <th>결근 횟수</th>
                    <th>상태</th>
                    <th>회원비<br>납부여부</th>
                    <th>관리 지점명</th>
                    <th>등록일자</th>
                </tr>
            </thead>

            <tbody class="tbody">
			    <%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>

        </table>                
        </div>  
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    <!-- pop_up -->
    <div class="pop_up">
          <div class="pop_inner workman_pop center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div class="top">
                    <div><span>일꾼 번호</span><input type="text" id="p_id" value="" disabled="disabled"></div>
                    <div><span>이름</span><input type="text" id="p_name" value=""></div>
                    <div><span>회원가입 승인여부</span><span class="submit_result status_flag_text">승인</span></div>
                    <input type="button" value="회원가입 승인" class="blue_btn join_submit"/>
               </div>
               <ul class="pop_content">
                    <li class="section_1">
                    	<div><span>email</span><input type="text" id="p_email" value="" disabled="disabled"></div>
                        <div><span>직업군</span><input type="text" id="" value=""  disabled="disabled"/></div>   
                        <div><span>성별</span>
                        	<select id="p_sex">
                        		<option value="">선택해주세요.</option>
                        		<option value="M">남자</option>
                        		<option value="W">여자</option>
                        	</select>
                        </div>   
                        <div><span>업종</span><input type="text" id="p_job_json" value="" disabled="disabled"/></div>   
                        <div><span>핸드폰 번호</span><input type="text" id="p_phone" value="" /></div>   
                        <div><span>주소</span><input type="text" id="p_user_location" value="" /></div> 
                        <div><span>장기근무 가능여부</span>
                        	<select id="p_long_time">
                        		<option value="">선택해주세요.</option>
                        		<option value="Y">가능</option>
                        		<option value="N">불가능</option>
                        	</select>
                        </div>  
                           <div><span>(현)장기근무 진행여부</span>
                        	<select id="p_job_long_ing">
                        		<option value="">선택해주세요.</option>
                        		<option value="Y">진행중</option>
                        		<option value="N">비진행중</option>
                        	</select>
                        </div>  
                   </li>
                   <li class="section_2">
                        <div><span>국적</span>
                        	<select id="p_country">
                        		<option value="">선택해주세요.</option>
                        		<option value="내국인">내국인</option>
                        		<option value="외국인">외국인</option>
                        	</select>
                        </div>   
                        <div><span>지원 건수</span><input type="text" id="p_register_count" value="" /></div>   
                        <div><span>배정 건수</span><input type="text" id="p_accept_count" value="" /></div>   
                        <div><span>취소 건수</span><input type="text" id="p_cancel_count" value="" /></div>   
                        <div><span>결근 횟수</span><input type="text" id="p_absent_count" value="" /></div>   
                   </li>
                   <li class="section_3">
                        <div><span>상태</span>
                        	<select id="p_type_flag">
                        		<option value="">선택해주세요.</option>
                        		<option value="0">일반</option>
                        		<option value="1">점주</option>
                        	</select>
                        </div>   
                        <div><span>회원비<br>납부 여부</span>
                       		<select id="p_prepaid_flag">
                        		<option value="">선택해주세요.</option>
                        		<option value="1">납부완료</option>
                        		<option value="0">납부안됨</option>
                        	</select>
                        </div>   
                        <div><span>관리 지점명</span>
                        	<select id="p_branch_id"></select>
                        </div>   
                        <div><span>등록일자</span><input type="text" id="p_create_date" value=""  disabled="disabled"/></div>    
                   </li>
               </ul>
              <div class="button">
                 <button type="button" class="modify-btn" data-id="">수정</button>
                 <button type="button" class="delete-btn" data-id="">삭제</button>
                 <button type="button" class="file-pop-btn" id="p2_health" data-id="" data-column-name="health" data-table-name="user">보건증 보기</button>
                 <button type="button" class="file-pop-btn" id="p2_license" data-id="" data-column-name="license" data-table-name="user">면허증 보기</button>
              </div>
           </div>
               <!-- join submit pop-->
            <div class="join_pop center inn_pop">
               <a href="#" class="inn_pop_close"><img src="/resources/images/close_btn.png" class="center"/></a>
                <p>회원 가입을 승인하시겠습니까?</p>
                <div>
                    <input type="submit" value="승인" class="blue_btn status-update-btn" data-status-flag="1"/>
                    <input type="button" value="비승인" class="status-update-btn" data-status-flag="0"/>
                </div>
            </div>   
       </div>
    <div class="long_term_pop">
        <div class="pop_inner center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <h5>장기일당 계약자 등록</h5>
               <ul class="long_term_content">
                    <li class="section_1">
                        <div><span>가맹점 상호명</span><input type="text" id="" value="" /></div>   
                        <div><span>일꾼 이름</span><input type="text" id="" value="" /></div>      
                   </li>
                   <li class="section_2">
                        <div><span>시작일자</span><input type="text" id="" value="" /></div>   
                        <div><span>종료일자</span><input type="text" id="" value="" /></div>     
                   </li>
               </ul>
              <div class="long_term_button">
                 <button type="button">등록</button>
                 <button type="button">취소</button>
              </div>
           </div>
    </div>   
  
    
