<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearchTime.jsp"%>
  <link rel="stylesheet" href="/resources/css/info.css" />
   <link rel="stylesheet" href="/resources/css/request1.css" />  
   <style>
   	#p_job_category{width: 20px;height: 20px;}
   </style>
<script>
	$(function(){
		/*검색 타입 변경시*/
		$(".content1 #search_type").change(function(){
			$(".content1 #keyword").focus();
		})
		
		/*검색 키워드 엔터시*/
		$(".content1 #keyword").keydown(function(key) {
            if (key.keyCode == 13) {
            	$(".content1 .search_btn").trigger("click");
            }
        });
		
		$(".content1 .search_btn").click(function(){
			globalpage1 = 1;
			init1(globalpage1);
		})
		
		$(".content2 #search_type").change(function(){
			$(".content2 #keyword").focus();
		})
		
		/*검색 키워드 엔터시*/
		$(".content2 #keyword").keydown(function(key) {
            if (key.keyCode == 13) {
            	$(".content2 .search_btn").trigger("click");
            }
        });
		
		$(".content2 .search_btn").click(function(){
			globalpage2 = 1;
			init2(globalpage2);
		})
		$(document).on("click", ".tr-radio", function(){
			$(this).find("input[type=radio]").attr("checked", true);
		})
	})
</script>
  
  
 <script>
 	var time1 = 10000;
	var poling1;
    var globalpage1 = 1;
    $(function(){
        init1(globalpage1);
        $(document).on("click", ".paging1 .gopage", function(){
			var page = $(this).attr("data-page");
			init1(page);
		})
    })
    function startPoling1(){
        poling1 = setInterval(function(){
            if(globalpage1 == 1){
                init1(globalpage1);
             }
        }, time1);
    }
    function endPoling1(){
        clearInterval(poling1)
    }
    function init1(page){
        globalpage1 = page;
        endPoling1();
        startPoling1();
        var param = {
       		"page":page,
       		"register_flag":"1",
       		"search_type":$(".content1 #search_type").val(),
       		"keyword":$(".content1 #keyword").val(),
       		"day_type":"request", 
       		"start_dt":searchToday(),
       		"end_dt":searchToday(),
       		"time_type":$(".content1 .time_type").val(),
       		"start_time":$(".content1 .start_time").val(),
       		"end_time":$(".content1 .end_time").val(),
       		"format":"%H:%i"
       		
        }
        ajaxCallPost("/api/v1/request/list", param, function(res){
            if(res.success){
            	$(".content1").find(".tbody").html($("#info_request_list").render(res.data));

                $(".paging1 .prev").attr("data-page", res.paging.prevPageNo);
                $(".paging1 .next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".paging1 .pageAll").html(str);
                
            }
        })
    }
</script> 
  <script>
  	var time2 = 10000;
	var poling2;
    var globalpage2 = 1;
    $(function(){
        init2(globalpage2);
        $(document).on("click", ".paging2 .gopage", function(){
			var page = $(this).attr("data-page");
			init2(page);
		})
    })
    function startPoling2(){
        poling2 = setInterval(function(){
            if(globalpage2 == 1){
                init2(globalpage2);
             }
        }, time2);
    }
    function endPoling2(){
        clearInterval(poling2)
    }
    function init2(page){
        globalpage2 = page;
        endPoling2();
        startPoling2();
        var param = {
       		"page":page,
       		"register_flag":"0",
       		"search_type":$(".content2 #search_type").val(),
       		"keyword":$(".content2 #keyword").val(),
       		"day_type":"apply",
       		"start_dt":searchToday(),
       		"end_dt":searchToday(),
       		"time_type":$(".content2 .time_type").val(),
       		"start_time":$(".content2 .start_time").val(),
       		"end_time":$(".content2 .end_time").val(),
       		"format":"%H:%i"
        }
        ajaxCallPost("/api/v1/apply/list", param, function(res){
            if(res.success){
                $(".content2").find(".tbody").html($("#info_apply_list").render(res.data));

                $(".paging2 .prev").attr("data-page", res.paging.prevPageNo);
                $(".paging2 .next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".paging2 .pageAll").html(str);
                
            }
        })
    }
    
    
    
    $(function(){
    	
    	$(document).on("dblclick", ".requestRead", function(e){
    		var id = $(this).attr("data-id");
    		
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/request/read", param, function(res){
    			if(res.success){
    				
    				var item = res.data.read;
    				$(".pop_up1 #p_id").val(item.id);
    				$(".pop_up1 #p_shop_id").val(item.shop_id);
    				$(".pop_up1 #p_shop_name").val(item.shop_name);
//     				$(".pop_up1 #p_job_category").val(global.job_format(item.job_category));
    				
    				var str = "";
    				for(var i = 0 ; i < globalJobArray.length ; i++){
    					str += '<div><input type="radio" name="p_job_category" id="p_job_category" value="'+i+'"  '+(i==item.job_category ? "checked":"")+'>'+globalJobArray[i]+"</div>";
    				}
    				$(".pop_up1 #p_job_category_div").html(str);
    				
    				
    				var status_falg_text = "";
					if(item.status_flag == 0){
						status_falg_text = "대기중"
					}else{
						status_falg_text = "배정완료"
					}    				
    				$(".pop_up1 #p_status_flag").val(status_falg_text);
    				$(".pop_up1 #p_pay").val(global.comma(item.pay));
    				$(".pop_up1 #p_shop_tel").val(global.hipen(item.shop_tel));
    				$(".pop_up1 #p_apply_date").val(item.apply_date);
    				$(".pop_up1 #p_accept_date").val(item.accept_date);
    				$(".pop_up1 #p_end_date").val(item.end_date);
    				$(".pop_up1 #p_user_id").val(item.user_id);
    				$(".pop_up1 #p_user_name").val(item.user_name);
    				$(".pop_up1 #p_user_phone").val(global.hipen(item.user_phone));
    				$(".pop_up1 #p_location").val(item.location);
    				$(".pop_up1 #p_branch_name").val(item.branch_name);
    				$(".pop_up1 #p_branch_tel").val(global.hipen(item.branch_tel));
    				
    				$(".pop_up1 .delete-btn").attr("data-id", item.id);
    				$(".pop_up1 .modify-btn").attr("data-work-id", item.id);
    				$(".pop_up1 .modify-btn").attr("data-shop-id", item.shop_id);
    				$(".pop_up1 .modify-btn").attr("data-user-id", item.user_id);
    				$(".pop_up1 .assig-call-btn").attr("data-id", item.id);
    				$('.pop_up1').css({"display" : "block"});
    			}
    		})
    	})
    	
    	$(document).on("dblclick", ".applyRead", function(e){
    		var id = $(this).attr("data-id");
    		
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/apply/read", param, function(res){
    			if(res.success){
    				
    				var item = res.data.read;
    				$(".pop_up2 #p_id").val(item.id);
    				$(".pop_up2 #p_shop_id").val(item.shop_id);
    				$(".pop_up2 #p_shop_name").val(item.shop_name);
//     				$(".pop_up2 #p_job_category").val(global.job_format(item.job_category));
    				
    				var str = "";
    				for(var i = 0 ; i < globalJobArray.length ; i++){
    					str += '<div><input type="radio" name="p_job_category" id="p_job_category" value="'+i+'"  '+(i==item.job_category ? "checked":"")+'>'+globalJobArray[i]+"</div>";
    				}
    				$(".pop_up2 #p_job_category_div").html(str);
    				
    				var status_falg_text = "";
					if(item.status_flag == 0){
						status_falg_text = "대기중"
					}else{
						status_falg_text = "배정완료"
					}    				
    				$(".pop_up2 #p_status_flag").val(status_falg_text);
    				$(".pop_up2 #p_pay").val(global.comma(item.pay));
    				$(".pop_up2 #p_shop_tel").val(global.hipen(item.shop_tel));
    				$(".pop_up2 #p_apply_date").val(item.apply_date);
    				$(".pop_up2 #p_accept_date").val(item.accept_date);
    				$(".pop_up2 #p_end_date").val(item.end_date);
    				$(".pop_up2 #p_user_id").val(item.user_id);
    				$(".pop_up2 #p_user_name").val(item.user_name);
    				$(".pop_up2 #p_user_phone").val(global.hipen(item.user_phone));
    				$(".pop_up2 #p_location").val(item.location);
    				$(".pop_up2 #p_branch_name").val(item.branch_name);
    				$(".pop_up2 #p_branch_tel").val(global.hipen(item.branch_tel));
    				
    				$(".pop_up2 .delete-btn").attr("data-id", item.id);
    				$(".pop_up2 .modify-btn").attr("data-work-id", item.id);
    				$(".pop_up2 .modify-btn").attr("data-shop-id", item.shop_id);
    				$(".pop_up2 .modify-btn").attr("data-user-id", item.user_id);
    				$(".pop_up2 .assig-call-btn").attr("data-id", item.id);
    				$('.pop_up2').css({"display" : "block"});
    			}
    		})
    	})
    	
    	$(document).on("click", ".pop_up1 .delete-btn", function(){
    		var id = $(this).attr("data-id");
    		common_delete(id, '1');
    	})
    	$(document).on("click", ".pop_up2 .delete-btn", function(){
    		var id = $(this).attr("data-id");
    		common_delete(id, '2');
    	})
    	$(document).on("click", ".pop_up1 .modify-btn", function(){
    		var work_id = $(this).attr("data-work-id");
    		var shop_id = $(this).attr("data-shop-id");
    		var user_id = $(this).attr("data-user-id");
    		var pay = $(".pop_up1  #p_pay");
    		var job_category = $(".pop_up1 input[name=p_job_category]:checked");
    		
    		var param = {
    				"work_id":work_id,
    				"shop_id":shop_id,
    				"user_id":user_id,
    				"pay":pay.val(),
    				"job_category":job_category.val()
    		}
    		ajaxCallPost("/api/v1/request/info/update", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
    				$( '.close_btn' ).trigger("click");
    				init1(globalpage1);
    				init2(globalpage2);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	$(document).on("click", ".pop_up2 .modify-btn", function(){
    		var work_id = $(this).attr("data-work-id");
    		var shop_id = $(this).attr("data-shop-id");
    		var user_id = $(this).attr("data-user-id");
    		var phone = $(".pop_up2  #p_user_phone");
    		var pay = $(".pop_up2  #p_pay");
    		var job_category = $(".pop_up2 input[name=p_job_category]:checked");
    		
    		var param = {
    				"work_id":work_id,
    				"shop_id":shop_id,
    				"user_id":user_id,
    				"phone":phone.val(),
    				"pay":pay.val(),
    				"job_category":job_category.val()
    		}
    		ajaxCallPost("/api/v1/apply/info/update", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
    				$( '.close_btn' ).trigger("click");
    				init1(globalpage1);
    				init2(globalpage2);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	
    	
    	
    	function common_delete(targer_id){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).attr("data-id");
	    		var param = {
	    				"table_name":"work",
	    				"id":targer_id
	    		}
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				$( '.close_btn' ).trigger("click");
	    				init1(globalpage1);
	    				init2(globalpage2);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    	}
    	
    	var globalpage3 = 1;
    	$(document).on("click", ".paging3_gopage", function(){
 			var page = $(this).attr("data-page");
 			init3(page);
 		})
 		$('.pop_up1 .assignment_btn').click(function(){
    		init3(1);    
   	    });
    	$('.pop_up1 .search_btn').click(function(){
    		init3(1);
   	    });
    	$('.pop_up1 #assig_keyword').keydown(function(key){
			 if (key.keyCode == 13) {
				init3(1);
            }
  	    });
    	$('.pop_up1 .reset-btn').click(function(key){
    		$(".pop_up1 #assig_keyword").val("");
    		init3(1);
  	    });
    	$(".pop_up1 .assig-call-btn").click(function(){
    		var work_id = $(this).attr("data-id");
    		var user_id = $("input[name=user_id]:checked").val();
    		
    		if(!user_id){
    			alert("배정할 일꾼을 선택해주세요.");
    			return;
    		}
    		var param = {
    	            "work_id":work_id,
    	            "user_id":user_id
    	        }
    	        ajaxCallPost("/api/v1/request/assign", param, function(res){
    	        	 if(res.success){
    	        		 alert("배정이 완료되었습니다.");
    	        		 $(".assignment_pop_close").trigger("click");
    	        		 $(".close_btn").trigger("click");
    	        		 init1(globalpage1);
    	            }
    	        })
  	    });
    	
 		function init3(page){
    		globalpage3 = page;
    		var param = {
    	            "page":page,
    	            "page_block":"10",
    	            "search_type":"user_name",
    	       		"keyword":$(".pop_up1 #assig_keyword").val(),
    	       		"sort_type":"user_name",
    	       		"sort_value":"desc",
    	       		"type_flag":"0"
    	        }
    	        ajaxCallPost("/api/v1/workman/list", param, function(res){
    	        	 if(res.success){
    	        		 
    	        		 $(".paging3 .prev").attr("data-page", res.paging.prevPageNo);
    	        		 $(".paging3 .prev").addClass("paging3_gopage");
    	                 $(".paging3 .next").attr("data-page", res.paging.nextPageNo);
    	        		 $(".paging3 .next").addClass("paging3_gopage");

    	                 var str = "";
    	                 for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

    	                     if(i == res.paging.page){
    	                     	str += "<a data-page=\""+i+"\" class=\"active paging3_gopage\">"+i+"</a>"
    	                     }else{
    	                     	str += "<a data-page=\""+i+"\" class=\"paging3_gopage\">"+i+"</a>"
    	                     }
    	                 }
    	                 $(".paging3 .pageAll").html(str);
    	        		 
    	        		 
    	        		 
    	        		 
    	                $(".workman-list").html($("#info_workman_list").render(res));
    	                $('.pop_up1 .assignment_pop').css({"display" : "block"});
    	       	        $('.pop_up1 .reauest_pop').css({"display" : "none"});
    	            }
    	        })
    	 }
 		
 		
 		var globalpage4 = 1;
    	$(document).on("click", ".paging4_gopage", function(){
 			var page = $(this).attr("data-page");
 			init4(page);
 		})
 		$('.pop_up2 .assignment_btn').click(function(){
    		init4(1);    
   	    });
 		$('.pop_up2 .search_btn').click(function(){
    		init4(1);
   	    });
 		$('.pop_up2 #assig_keyword').keydown(function(key){
 			 if (key.keyCode == 13) {
 				init4(1);
             }
   	    });
 		$('.pop_up2 .reset-btn').click(function(key){
    		$(".pop_up2 #assig_keyword").val("");
    		init4(1);
  	    });
 		
 		$(".pop_up2 .assig-call-btn").click(function(){
    		var work_id = $(this).attr("data-id");
    		var shop_id = $("input[name=shop_id]:checked").val();
    		var user_id = $("input[name=shop_id]:checked").attr("data-user-id");
    		
    		if(!shop_id){
    			alert("배정할 가맹점을 선택해주세요.");
    			return;
    		}
    		var param = {
    	            "work_id":work_id,
    	            "shop_id":shop_id,
    	            "user_id":user_id
    	        }
    	        ajaxCallPost("/api/v1/apply/assign", param, function(res){
    	        	 if(res.success){
    	        		 alert("배정이 완료되었습니다.");
    	        		 $(".assignment_pop_close").trigger("click");
    	        		 $(".close_btn").trigger("click");
    	        		 init2(globalpage2);
    	            }
    	        })
  	    });
 		function init4(page){
    		globalpage4 = page;
    		var param = {
    	            "page":page,
    	            "page_block":"10",
    	            "search_type":"shop_name",
    	       		"keyword":$(".pop_up2 #assig_keyword").val(),
    	       		"sort_type":"shop_name",
    	       		"sort_value":"desc"
    	        }
    	        ajaxCallPost("/api/v1/franchisee/list", param, function(res){
    	        	 if(res.success){
    	        		 
    	        		 $(".paging4 .prev").attr("data-page", res.paging.prevPageNo);
    	        		 $(".paging4 .prev").addClass("paging4_gopage");
    	                 $(".paging4 .next").attr("data-page", res.paging.nextPageNo);
    	        		 $(".paging4 .next").addClass("paging4_gopage");

    	                 var str = "";
    	                 for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

    	                     if(i == res.paging.page){
    	                     	str += "<a data-page=\""+i+"\" class=\"active paging4_gopage\">"+i+"</a>"
    	                     }else{
    	                     	str += "<a data-page=\""+i+"\" class=\"paging4_gopage\">"+i+"</a>"
    	                     }
    	                 }
    	                 $(".paging4 .pageAll").html(str);
    	        		 
    	        		 
    	                $(".shop-list").html($("#info_shop_list").render(res.data));
    	                $('.pop_up2 .assignment_pop').css({"display" : "block"});
    	       	        $('.pop_up2 .reauest_pop').css({"display" : "none"});
    	            }
    	        })
    	 }
    })
</script>
  
  
  <div class="content">
        <div class="fees_content">
            <div class="shop_fees content1">
                <div class="sub_title"><span></span>금일 요청내역</div>
                
                
                
                
                
                 <div class="search_area">
		            <div class="search">
		                <span>검색</span>
		                <select id="search_type" name="sort">
		                    <option value="">전체</option>
		                    <option value="work_id">접수번호</option>
		                    <option value="shop_name">상호명</option>
		                    <option value="user_name">일꾼 이름</option>
		                    <option value="user_phone">일꾼 전화번호</option>
		                </select>
		                <div class="search_txt">
		                    <input type="text" name="keyword" id="keyword" value="" />
		                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
		                </div>
		            </div>  
		            
		            <div class="time_search">
		                <span>시간 검색</span>
		                <select id="" name="" class="option time_type">
		                    <option value="">전체</option>
		                    <option value="apply_date">접수 시간</option>
		                    <option value="accept_date">배정 시간</option>
		                </select>
		                <select id="" name="" class="option_time start_time">
		                    <option value="">시작</option>
		                    <option value="09:00">09:00</option>
		                    <option value="10:00">10:00</option>
		                    <option value="11:00">11:00</option>
		                    <option value="12:00">12:00</option>
		                    <option value="13:00">13:00</option>
		                    <option value="14:00">14:00</option>
		                    <option value="15:00">15:00</option>
		                    <option value="16:00">16:00</option>
		                    <option value="17:00">17:00</option>
		                    <option value="18:00">18:00</option>
		                </select>
		                ~
		                <select id="" name="" class="option_time end_time">
		                <option value="">종료</option>
		                    <option value="09:00">09:00</option>
		                    <option value="10:00">10:00</option>
		                    <option value="11:00">11:00</option>
		                    <option value="12:00">12:00</option>
		                    <option value="13:00">13:00</option>
		                    <option value="14:00">14:00</option>
		                    <option value="15:00">15:00</option>
		                    <option value="16:00">16:00</option>
		                    <option value="17:00">17:00</option>
		                    <option value="18:00">18:00</option>
		                </select>
		                <input type="button" name="" id="" value="검색" class="blue_btn search-time">
		            </div> 
		        </div>   
                
                
                
                
                
                <table>
                    <thead>
                        <tr>
                            <th class="number">NO.</th>
		                    <th>접수 번호</th>
		                    <th>상호명</th>
		                    <th>상태</th>
		                    <th>접수시간</th>
		                    <th>배정시간</th>
		                    <th>일꾼명</th>
		                    <th>일꾼전화번호</th>
                        </tr>
                    </thead>
	                   <tbody class="tbody">
	                    </tbody>
                </table>
                
                
                <!-- 페이지 번호 -->     
                <div class="paging1">
                	<%@ include file="/WEB-INF/views/page/paging.jsp"%>
                </div>     
                
             </div>
             
             
             
             
             
             <div class="workman_fees content2">
                <div class="sub_title"><span></span>금일 지원내역</div>
                
                
                   <div class="search_area">
		            <div class="search">
		                <span>검색</span>
		                <select id="search_type" name="sort">
		                    <option value="">전체</option>
		                    <option value="work_id">접수번호</option>
		                    <option value="user_name">일꾼 이름</option>
		                    <option value="user_phone">일꾼 전화번호</option>
		                </select>
		                <div class="search_txt">
		                    <input type="text" name="keyword" id="keyword" value="" />
		                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
		                </div>
		            </div>  
		            
		            <div class="time_search">
		                <span>시간 검색</span>
		                <select id="" name="" class="option time_type">
		                    <option value="">전체</option>
		                    <option value="apply_date">접수 시간</option>
		                    <option value="accept_date">배정 시간</option>
		                </select>
		                <select id="" name="" class="option_time start_time">
		                <option value="">시작</option>
		                    <option value="09:00">09:00</option>
		                    <option value="10:00">10:00</option>
		                    <option value="11:00">11:00</option>
		                    <option value="12:00">12:00</option>
		                    <option value="13:00">13:00</option>
		                    <option value="14:00">14:00</option>
		                    <option value="15:00">15:00</option>
		                    <option value="16:00">16:00</option>
		                    <option value="17:00">17:00</option>
		                    <option value="18:00">18:00</option>
		                </select>
		                ~
		                <select id="" name="" class="option_time end_time">
		                <option value="">종료</option>
		                    <option value="09:00">09:00</option>
		                    <option value="10:00">10:00</option>
		                    <option value="11:00">11:00</option>
		                    <option value="12:00">12:00</option>
		                    <option value="13:00">13:00</option>
		                    <option value="14:00">14:00</option>
		                    <option value="15:00">15:00</option>
		                    <option value="16:00">16:00</option>
		                    <option value="17:00">17:00</option>
		                    <option value="18:00">18:00</option>
		                </select>
		                <input type="button" name="" id="" value="검색" class="blue_btn search-time">
		            </div> 
		        </div>  
                
                
                
                <table>
                    <thead>
                        <tr>
                            <th class="number">NO.</th>
		                    <th>접수 번호</th>
		                    <th>상태</th>
		                    <th>급료</th>
		                    <th>접수시간</th>
		                    <th>배정시간</th>
		                    <th>일꾼명</th>
		                    <th>일꾼 전화번호</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                    </tbody>
                </table>
                
                
                <!-- 페이지 번호 -->          
                <div class="paging2">
                	<%@ include file="/WEB-INF/views/page/paging.jsp"%>
                </div>     
                
                
                
             </div>
        </div>               
    </div>
    
    
    
    
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up pop_up1">
    		<!-- 요청정보 팝업 -->
           <div class="pop_inner reauest_pop center info">
            <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
            <div class="left">
                <h2>상점정보</h2>
                <ul>
                    <li>
                        <p class="list_title">가맹점 번호</p>
                        <div><input type="text" value="" id="p_shop_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상호명</p>
                        <div><input type="text" value="" id="p_shop_name" disabled="disabled"></div>
                    </li>
                    <li class="job_category">
                        <p class="list_title">업종</p>
                        <div id="p_job_category_div">
                        </div>
                    </li>
                    <li>
                        <p class="list_title">급료</p>
                        <div><input type="text" value="" id="p_pay"></div>
                    </li>
                    <li>
                        <p class="list_title">전화번호</p>
                        <div><input type="text" value="" id="p_shop_tel" disabled="disabled"></div>
                    </li>
                    <li class="address">
                        <p class="list_title">주소</p>
                        <div><input type="text" value="" id="p_location" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점명</p>
                        <div><input type="text" value="" id="p_branch_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점<br>전화번호</p>
                        <div><input type="text" value="" id="p_branch_tel" disabled="disabled"></div>
                    </li>
                </ul>
            </div>
        
            <div class="right">
                <h2>일꾼정보</h2>
                <ul>
                    <li>
                        <p class="list_title">접수번호</p>
                        <div><input type="text" value="" id="p_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼번호</p>
                        <div><input type="text" value="" id="p_user_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼명</p>
                        <div><input type="text" value="" id="p_user_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼전화번호</p>
                        <div><input type="text" value="" id="p_user_phone" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상태</p>
                        <div><input type="text" value="" id="p_status_flag" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">접수시간</p>
                        <div><input type="text" value="" id="p_apply_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">경과시간</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">배정시간</p>
                        <div><input type="text" value="" id="p_accept_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">완료시간</p>
                        <div><input type="text" value="" id="p_end_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">결제방식</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
    
                </ul>
            </div>
    
            <div class="button">
                <button type="button" class="modify-btn" data-work-id="" data-shop-id="" data-user-id="">수정</button>
                <button type="button" class="delete-btn">삭제</button>
                <button type="button" class="assignment_btn">배정</button>
            </div>
        </div>
           
        <!-- 배정 -->
        <div class="shop_info_wrap">
	        <div class="assignment_pop center">
	            <div class="search_txt">
                    <input type="text" name="assig_keyword" id="assig_keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
	           <a href="#" class="assignment_pop_close"><img src="/resources/images/close_btn.png" class="center"/></a>
	            <table>
	                <thead>
	                    <tr>
	                        <th class="number">
	                            <div class="checks">
	                                  <label for="check2"><span>선택</span></label> 
	                            </div>
	                        </th>
	                        <th>일꾼명(요청)</th>
	                        <th>일꾼 전화번호</th>
	                    </tr>
	                </thead>
	                <tbody class="workman-list">
	                    
	                </tbody>
	            </table>
	            <!-- 페이지 번호 -->          
                <div class="paging3">
                	<%@ include file="/WEB-INF/views/page/paging.jsp"%>
                </div> 
	            <div>
	                <input type="submit" value="확인" class="blue_btn assig-call-btn" data-id=""/>
	                <input type="button" value="취소" class="reset-btn"/>
	            </div>
	       </div>
	    </div>
	</div>
    
    
    
    <!-- pop_up -->
    <div class="pop_up pop_up2">
        
           <!-- 지원정보 팝업 -->
           <div class="pop_inner reauest_pop center info">
            <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
            <div class="left">
                <h2>일꾼정보</h2>
                <ul>
                    <li>
                        <p class="list_title">접수번호</p>
                        <div><input type="text" value="" id="p_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼번호</p>
                        <div><input type="text" value="" id="p_user_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼명</p>
                        <div><input type="text" value="" id="p_user_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">일꾼전화번호</p>
                        <div><input type="text" value="" id="p_user_phone"></div>
                    </li>
                    <li class="job_category">
                        <p class="list_title">업종</p>
                        <div id="p_job_category_div"></div>
                    </li>
                    <li>
                        <p class="list_title">급료</p>
                        <div><input type="text" value="" id="p_pay"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점명</p>
                        <div><input type="text" value="" id="p_branch_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">관리지점<br>전화번호</p>
                        <div><input type="text" value="" id="p_branch_tel" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상태</p>
                        <div><input type="text" value="" id="p_status_flag" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">접수시간</p>
                        <div><input type="text" value="" id="p_apply_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">경과시간</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">배정시간</p>
                        <div><input type="text" value="" id="p_accept_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">완료시간</p>
                        <div><input type="text" value="" id="p_end_date" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">결제방식</p>
                        <div><input type="text" value="" id="" disabled="disabled"></div>
                    </li>
                </ul>
            </div>
        
            <div class="right">
                <h2>상점정보</h2>
                <ul>
    				<li>
                        <p class="list_title">가맹점 번호</p>
                        <div><input type="text" value="" id="p_shop_id" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">상호명</p>
                        <div><input type="text" value="" id="p_shop_name" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">전화번호</p>
                        <div><input type="text" value="" id="p_shop_tel" disabled="disabled"></div>
                    </li>
                    <li>
                        <p class="list_title">주소</p>
                        <div><input type="text" value="" id="p_location" disabled="disabled"></div>
                    </li>
                </ul>
            </div>
    
            <div class="button">
                <button type="button" class="modify-btn" data-work-id="" data-shop-id="" data-user-id="">수정</button>
                <button type="button" class="delete-btn">삭제</button>
                <button type="button" class="assignment_btn">배정</button>
            </div>
        </div>
           
       
       
       
       
       
       
       <!-- 배정 -->
       <div class="assignment_pop center">
       		<div class="search_txt">
               <input type="text" name="assig_keyword" id="assig_keyword" value="" />
               <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
           </div>
           <a href="#" class="assignment_pop_close"><img src="/resources/images/close_btn.png" class="center"/></a>
            <table>
                <thead>
                    <tr>
                        <th class="number">
                            <div class="checks">
                                  <label for="check2"><span>선택</span></label> 
                            </div>
                        </th>
                        <th>가맹점(지원)</th>
                        <th>전화번호</th>
                    </tr>
                </thead>
                <tbody class="shop-list">
                </tbody>
            </table>
            <!-- 페이지 번호 -->          
            <div class="paging4">
            	<%@ include file="/WEB-INF/views/page/paging.jsp"%>
            </div> 
            <div>
                <input type="submit" value="확인" class="blue_btn assig-call-btn" data-id=""/>
                <input type="button" value="취소" class="reset-btn"/>
            </div>
       </div>
    </div>
    
    
