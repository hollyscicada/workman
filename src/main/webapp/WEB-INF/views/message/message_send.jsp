<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/message_send.css" />
      <script>
	      var globalpage = 1;
			$(function () {
				init(globalpage);
				$(document).on("click", ".gopage", function(){
					var page = $(this).attr("data-page");
					init(page);
				})
				
				$(document).on("click", ".type-flag", function(){
					init(1);
				})
				
				$(document).on("click", ".all_chk", function(){
					
					var all = $(".all_chk").prop("checked");
					if(all){
						$(".chk").prop("checked", true);
					}else{
						$(".chk").prop("checked", false);
					}
				})
				$(document).on("click", ".all_chk2", function(){
					
					var all = $(".all_chk2").prop("checked");
					if(all){
						$(".chk2").prop("checked", true);
					}else{
						$(".chk2").prop("checked", false);
					}
				})
				$(document).on("click", ".minus-btn img", function(){
					var chks2 = $(".chk2");
					for(var i = 0 ; i < chks2.length ; i++){
						if(chks2.eq(i).prop("checked")){
							chks2.eq(i).parents("tr").remove();
						}
					}
				})
				$(document).on("click", ".plus-btn img", function(){
					var chks = $(".chk");
					
					var html = "";
					for(var i = 0 ; i < chks.length ; i++){
	     	        	if(chks.eq(i).prop("checked")){
	     	        		
	     	        		var chks2 = $(".chk2");
	     	        		var is = true;
	     	        		for(var x = 0 ; x < chks2.length ; x++){
	     	        			if(chks2.eq(x).val() == chks.eq(i).val()){
	     	        				is = false;
	     	        			}
	     	        		}
	     	        		
	     	        		
	     	        		if(is){
     	        				var tr = "<tr>"+chks.eq(i).parents("tr").html()+"</tr>".replace("chk", "chk2");
     	        				tr = tr.replace("chk", "chk2");
    	     	        		html+=tr;
     	        			}
	     	        	}
	     	        }
					
					if($(".tbody2").html().trim()){
						$(".tbody2").find("tr:last-child").after(html);
					}else{
						$(".tbody2").html(html)
					}
					
					
				})
				
				
				$(".cancel-btn").click(function(){
					$("input[name=title]").val("");
					$("textarea[name=content]").val("");
				})
				$(".submit-btn").click(function(){
	    			
	    			var title = $("input[name=title]");
	    			var content = $("textarea[name=content]");
	    			
	    			if(!title.val()){
	    				alert("제목을 입력해주세요.");
	    				title.focus();
	    				return;
	    			}
	    			if(!content.val()){
	    				alert("내용을 입력해주세요.");
	    				content.focus();
	    				return;
	    			}
	    			
	    			var chks2 = $(".chk2");
	    			if(!chks2.length){
	    				alert("수신자를 지정해주세요.")
	    				return;
	    			}
	    			
	    			var ids = [];
	    			for(var i = 0 ; i < chks2.length ; i++){
	    				var param = {
	    						"id":chks2.eq(i).val()
	    				}
	    				ids.push(param);
	    			}
	    			
	    			var param = {
	    					"title":title.val(),
	    					"sender_id":"${sessionScope.MEMBER.id }",
	    					"ids":ids,
	    					"content":content.val()
	    			}
	    			ajaxCallPost("/api/v1/message/create", param, function(res){
	    				if(res.success){
	    					alert("해당 데이터를 추가하였습니다.");
	    					location.href="/supervise/message"
	    				}else{
	    					alert("등록에 실패하였습니다.")
	    				}
	    			})
	    		})
	    		
				
				
			});
	      
     	  function init(page){
     		 	$(".all_chk").prop("checked", false);
     	        globalpage = page;
     	        
     	        var type_flags = $(".type-flag");
     	        var type_flag = "";
     	        for(var i = 0 ; i < type_flags.length ; i++){
     	        	if(type_flags.eq(i).attr("class").indexOf("on") > -1){
     	        		type_flag = type_flags.eq(i).attr("data-type-flag");
     	        	}
     	        }
     	        
     	        var param = {
     	            "page":page,
     	       		"type_flag":type_flag,
	     	       	"search_type":"email",
	           		"keyword":$("#keyword").val()
	           		
     	        }
     	        ajaxCallPost("/api/v1/workman/list", param, function(res){
     	        	 if(res.success){
     	                $(".tbody").html($("#message_user").render(res));

     	                $(".prev").attr("data-page", res.paging.prevPageNo);
     	                $(".next").attr("data-page", res.paging.nextPageNo);

     	                var str = "";
     	                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

     	                    if(i == res.paging.page){
     	                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
     	                    }else{
     	                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
     	                    }
     	                }
     	                $(".pageAll").html(str);
     	            }
     	        })
     	   }
      </script>
      
    <div class="content">
        <div class="search_area">
           <div class="mess_top_btn">
                <button class="type-flag on" data-type-flag="">전체</button>
                <button class="type-flag" data-type-flag="1">점주</button>
                <button class="type-flag" data-type-flag="0">일꾼</button>
           </div>
            <div class="search">
                <span>검색</span>
                <div class="search_txt">
                    <input type="text" id="keyword" placeholder="명칭을 입력하세요."/>
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>  
        </div>
        <div class="id_list">
            <table>
                <thead>
                    <tr>
                        <th class="number"><input type="checkbox" class="all_chk"></th>
                        <th class="number">NO.</th>
                        <th>아이디</th>
                    </tr>
                </thead>

                <tbody class="tbody">
                </tbody>
            </table> 
            
            
            
            
            <table>
                <thead>
                    <tr>
                    	<th class="number"><input type="checkbox" class="all_chk2"></th>
                        <th class="number">NO.</th>
                        <th>아이디</th>
                    </tr>
                </thead>

                <tbody class="tbody2">
                </tbody>
            </table>
            
            <!-- 페이지 번호 -->          
            <%@ include file="/WEB-INF/views/page/paging.jsp"%>
            
            
            <div class="add_btn center">
                <button class="add plus-btn"><img src="/resources/images/add_btn.png" alt="아이디 추가"></button>
                <button class="subtract minus-btn"><img src="/resources/images/subtract_btn.png" alt="아이디 빼기"></button>
            </div>
            
        </div> 
        <div class="send_area">
            <div>
                <p>제목</p>
                <input type="text" name="title" class="send_title" />
            </div>
            <div>
                <p>내용</p>
                <textarea name="content" style="resize: none;"></textarea>
            </div>
            <div class="send_btn">
               <button class="blue_btn submit-btn">보내기</button>
               <button class="cancel cancel-btn">취소</button>
           </div>
        </div>              
    </div>

