<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
<link rel="stylesheet" href="/resources/css/message.css" />
<script>
    var globalpage = 1;
    $(function(){
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    })
    function init(page){
        globalpage = page;
        var param = {
       		"page":page,
       		"search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val(),
       		"day_type":"message",
       		"start_dt":$("input[name=start_dt]").val(),
       		"end_dt":$("input[name=end_dt]").val()
        }
        ajaxCallPost("/api/v1/message/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#message_list").render(res.data));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
            }
        })
    }
    
    
    $(function(){
    	
    	/*상세보기 팝업*/
    	$(document).on("dblclick", ".messageRead", function(e){
    		var id = $(this).attr("data-id");
    		
    		var param = {
    			"id":id
    		}
    		ajaxCallPost("/api/v1/message/read", param, function(res){
    			if(res.success){
    				var item = res.data.read;	
       				$(".p_id").text(item.id);
       				$(".p_sender_name").text(item.sender_name);
       				$(".p_receiver_name").text(item.receiver_name);
       				$(".p_create_date").text(item.create_date);
       				$(".p_content").val(item.content);
       				
    				$('.pop_up').css({"display" : "block"});
    			}
    		})
    	})
    	
    	/*답장히기 버튼 누를시*/
    	$(document).on("click", ".answer-btn", function(e){
    		var sender_id = $(this).attr("data-id");
    		$(".save-btn").attr("data-id", sender_id);
			$('.reply_pop').css({"display" : "block"});
    		
    	})
    	
    	/*답장하기 클릭시*/
    	$(".save-btn").click(function(){
    		var receiver_id = $(this).attr("data-id");
			var title = $("input[name=r_title]");
			var content = $("textarea[name=r_content]");
    		var param = {
					"title":title.val(),
					"content":content.val(),
					"receiver_id":receiver_id,
					"type":"A",
					"sender_id":"${sessionScope.MEMBER.id }"
			}
			ajaxCallPost("/api/v1/message/answer", param, function(res){
				if(res.success){
					alert("답변을 추가하였습니다.");
					$( '.close_btn' ).trigger("click");
					init(globalPage)
				}else{
					alert("답변등록에 실패하였습니다.")
				}
			})
    	})
    	
    })
    
</script> 
    <div class="content">
        <div class="search_area">
           <div class="send_btn">
                <a href="/supervise/message/send" class="blue_btn">메세지 보내기</a>
           </div>
            <div class="search">
                <span>검색</span>
                <select id="search_type" name="sort">
                    <option value="">전체</option>
                    <option value="sender_name">발신자</option>
                    <option value="receiver_name">수신자</option>
                </select>
                <div class="search_txt">
                    <input type="text" name="keyword" id="keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>  
            <%@ include file="/WEB-INF/views/common/listSearchDay.jsp"%>
        </div>   
        <table>
            <thead>
                <tr>
                    <th class="number">NO.</th>
                    <th>번호</th>
                    <th>발신자</th>
                    <th>수신자</th>
                    <th>제목</th>
                    <th>답장하기</th>
                    <th>날짜</th>
                </tr>
            </thead>

            <tbody class="tbody">
            	<%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>

        </table>                  
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up">
          <div class="pop_inner message_pop center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div class="top">
                   <div class="pop_no"><span>NO.</span><span class="p_id"></span></div>
                   <div><span>발신자</span><span class="p_sender_name"></span></div>
                   <div><span>수신자</span><span class="p_receiver_name"></span></div>
                   <div><span>날짜</span><span class="p_create_date"></span></div>
               </div>
               <div class="pop_content">
                   <div class="pop_txt"><textarea name="p_content" class="p_content" style="width: 100%" rows="" cols=""></textarea> </div>
               </div>
           </div>
       </div>
       <div class="reply_pop">
          <div  class="pop_inner reply_message center">
              <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div>
                   <p>제목</p>
                   <input class="p2_title" name="r_title">
               </div>
               <div>
                   <p>내용</p>
                   <textarea class="p2_content" name="r_content"></textarea>
               </div>
               <div class="pop_reply_btn">
                   <button class="blue_btn save-btn">보내기</button>
               </div>
            </div>   
       </div>
  
    
 
      <footer>
      </footer>
    </body>
</html>