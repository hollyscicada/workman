<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/banner.css" />
    
<script>
    var globalpage = 1;
    $(function(){
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    })
    function init(page){
        globalpage = page;
        var param = {
       		"page":page,
       		"search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val()
        }
        ajaxCallPost("/api/v1/banner/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#banner_list").render(res.data));

                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                    if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);
            }
        })
    }
    
    $(function(){
    	
       	$(document).on("dblclick", ".bannerRead", function(e){
       		var id = $(this).attr("data-id");
       		var param = {
       			"id":id
       		}
       		
       		ajaxCallPost("/api/v1/banner/read", param, function(res){
       			if(res.success){
       				var item = res.data.read;	
          				$("#p_type").val(item.type);
          				$("#p_file_url").val(item.file_url);
          				$("#p_sort").val(item.sort);
          				$("#p_callback_url").val(item.callback_url);
          				$("#p_file_url").attr("data-id", item.id);
          				$("#p_create_date").val(item.create_date);
          				
       				$('.pop_up').css({"display" : "block"});
       			}
       		})
       	})
        	
    	
    	$(document).on("click", ".update-btn", function(){
    		var id = $(this).parents("tr").attr("data-id");
    		location.href="/supervise/banner/update/"+id;
    	})
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).parents("tr").attr("data-id");
	    		var param = {
	    				"table_name":"banner",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    })
</script>
    <div class="content">
        <div class="search_area">
            <div class="new_notice">
                <a href="/supervise/banner/write" class="blue_btn">배너 등록</a>
            </div>
            <div class="search">
                <span>검색</span>
                <select id="search_type" name="sort">
                    <option value="">전체</option>
                    <option value="banner_id">번호</option>
                </select>
                <div class="search_txt">
                    <input type="text" name="keyword" id="keyword" value="" />
                    <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                </div>
            </div>

        </div>
        <div class="table_wrap">   
        <table>
            <thead>
                <tr class="table_more">
                    <th class="number">NO.</th>
                    <th style="width:200px">번호</th>
                    <th>파일</th>
                    <th>순서</th>
                    <th>콜백URL</th>
                    <th>작성일</th>
                    <th>수정 / 삭제</th>
                </tr>
            </thead>

            <tbody class="tbody">
            	<%@ include file="/WEB-INF/views/common/default.jsp"%>
            </tbody>

        </table>                
        </div>  
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    
    
    
    
    
    
    <!-- pop_up -->
    <div class="pop_up bnn_pop">
          <div class="pop_inner center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <ul class="pop_content">
                    <li class="section_1">
		                   <div><span>타입</span><input type="text" id="p_type" value="" disabled="disabled"></div>
		                   <div><span>순서</span><input type="text" id="p_sort" value="" disabled="disabled"></div>
                   </li>
                    <li class="section_1">
                        <div><span>콜백URL</span><input type="text" id="p_callback_url" value=""  disabled="disabled"/></div>   
                        <div><span>작성일</span><input type="text" id="p_create_date" value="" disabled="disabled" /></div>   
                   </li>
               </ul>
               
               <div class="button">
                 <button type="button" class="file-pop-btn" id="p_file_url" data-id="" data-column-name="file_url" data-table-name="banner">첨부파일</button>
              </div>
           </div>
       </div>
