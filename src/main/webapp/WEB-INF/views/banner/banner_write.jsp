<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/banner_write.css" />
    <script>
    	$(function(){
    		$(".submit-btn").click(function(){
    			
    			var sort = $("input[name=sort]");
    			var callback_url = $("input[name=callback_url]");
    			var file_url = $("input[name=file_url]");
    			
    			if(!sort.val()){
    				alert("순서를 입력해주세요.");
    				title.focus();
    				return;
    			}
    			
    			var param = {
    					"sort":sort.val(),
    					"callback_url":callback_url.val(),
    					"file_url":file_url.val()
    			}
    			ajaxCallPost("/api/v1/banner/create", param, function(res){
    				if(res.success){
    					alert("해당 데이터를 추가하였습니다.");
    					location.href="/supervise/banner"
    				}else{
    					alert("등록에 실패하였습니다.")
    				}
    			})
    		})
    		
    		$("input[name='file']").change(previewImgCreateFront);
    	})
    </script>
        <div class="content">            
            <ul class="write_area">
                <li>
                    <p>순서</p>
                    <input type="number" name="sort" />
                </li>
                <li>
                    <p>콜백URL</p>
                    <input type="text" name="callback_url" />
                </li>
                <li>
                    <p>첨부파일</p>
                    <div class='file-input'>
                      <input type='file' name="file">
                      <input type="hidden" name="file_url">
                      <span class='button'>파일 찾기</span>
                      <span class='label file_url_text' data-js-label>선택된 파일이 없습니다.</span>
                    </div>
                </li>
            </ul>
            <div class="submit_btn">
                <input type="button" value="목록보기" onclick="javascript:location.href='/supervise/banner'" />
                <input type="submit" value="배너 등록" class="blue_btn submit-btn"/>
            </div>
        </div>
