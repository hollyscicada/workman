<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
<%@ include file="/WEB-INF/views/common/listSearch.jsp"%>
    <link rel="stylesheet" href="/resources/css/spot.css" />
    <script>
    var time = 5000;
	var poling;
    var globalpage = 1;
    $(function () {
        init(globalpage);
        $(document).on("click", ".gopage", function(){
			var page = $(this).attr("data-page");
			init(page);
		})
    });
    function startPoling(){
        poling = setInterval(function(){
            if(globalpage == 1){
                init(globalpage);
             }
        }, time);
    }
    function endPoling(){
        clearInterval(poling)
    }
    function init(page){
        globalpage = page;
        endPoling();
        startPoling();
         var param = {
            "page":page,
            "search_type":$("#search_type").val(),
       		"keyword":$("#keyword").val(),
       		"sort_type":$("#sort_type").val(),
       		"sort_value":globalSortValue
        }
        ajaxCallPost("/api/v1/spot/list", param, function(res){
            if(res.success){
                $(".tbody").html($("#spot_list").render(res));


                $(".prev").attr("data-page", res.paging.prevPageNo);
                $(".next").attr("data-page", res.paging.nextPageNo);

                var str = "";
                for(var i = res.paging.startPageNo ; i <= res.paging.endPageNo ; i++){

                	if(i == res.paging.page){
                    	str += "<a data-page=\""+i+"\" class=\"active gopage\">"+i+"</a>"
                    }else{
                    	str += "<a data-page=\""+i+"\" class=\"gopage\">"+i+"</a>"
                    }
                }
                $(".pageAll").html(str);




            }
        })
    }
    
    
    $(function(){
    	$(document).on("dblclick", ".spotRead", function(e){
    		var id = $(this).attr("data-id");
    		var param = {
    			"id":id
    		}
    		
    		ajaxCallPost("/api/v1/spot/read", param, function(res){
    			if(res.success){
    				var item = res.data.read;	
       				$("#p_id").val(item.id);
       				$("#p_name").val(item.name);
       				$("#p_ceo_name").val(global.hipen(item.ceo_name));
       				$("#p_tel").val(global.hipen(item.tel));
       				$("#p_phone").val(global.hipen(item.phone));
       				$("#p_location").val(item.location);
       				$("#p_shop_cnt").val(item.shop_cnt);
       				$("#p_business_code").val(item.business_code);
       				$("#p_user_cnt").val(item.user_cnt);
       				
       				$("#p2_business").attr("data-id", item.id);
       				$(".modify-btn").attr("data-id", item.id);
       				$(".delete-btn").attr("data-id", item.id);
       				
    				$('.pop_up').css({"display" : "block"});
    			}
    		})
    	})
    	
    	
    	
    	
    	$(document).on("click", ".modify-btn", function(){
    		var id = $(this).attr("data-id");
    		var name = $("#p_name");
    		var ceo_name = $("#p_ceo_name");
    		var tel = $("#p_tel");
    		var phone = $("#p_phone");
    		var location = $("#p_location");
    		var business_code = $("#p_business_code");
    		
    		var param = {
    				"id":id,
    				"name":name.val(),
    				"ceo_name":ceo_name.val(),
    				"tel":tel.val(),
    				"phone":phone.val(),
    				"location":location.val(),
    				"business_code":business_code.val()
    		}
    		ajaxCallPost("/api/v1/spot/update", param, function(res){
    			if(res.success){
    				alert("해당 데이터를 수정하였습니다.")
    				$( '.close_btn' ).trigger("click");
    				init(globalpage);
    			}else{
    				alert("수정에 실패하였습니다.")
    			}
    		})
    	})
    	$(document).on("click", ".delete-btn", function(){
    		if(confirm("삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
	    		var id = $(this).attr("data-id");
	    		var param = {
	    				"table_name":"branch",
	    				"id":id
	    		}
	    		
	    		ajaxCallPost("/api/v1/common/remove", param, function(res){
	    			if(res.success){
	    				alert("해당 데이터를 삭제하였습니다.")
	    				$( '.close_btn' ).trigger("click");
	    				init(globalpage);
	    			}else{
	    				alert("삭제에 실패하였습니다.")
	    			}
	    		})
    		}
    		
    	})
    })
    
    

</script>
    <div class="content">
            <div class="search_area">
                <div class="search">
                    <span>검색</span>
                    <select id="search_type" name="sort">
                        <option value="">전체</option>
                        <option value="branch_id">지점 번호</option>
                        <option value="branch_name">지점명</option>
<!--                         <option value="">대표자명</option> -->
                        <option value="branch_tel">연락처</option>
                        <option value="branch_phone">핸드폰 번호</option>
                        <option value="branch_business_code">사업자 번호</option>
                    </select>
                    <div class="search_txt">
                        <input type="text" name="keyword" id="keyword" value="" />
                        <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                    </div>
                </div>  
                <div class="order_search">
                    <span>정렬</span>
                    <select id="sort_type" name="" class="option">
                        <option value="branch_id">전체</option>
                        <option value="shop_cnt">가맹점 수</option>
                        <option value="user_cnt">일꾼 수</option>
                        <option value="4">매출</option>
                    </select>
                    <div class="order_btn">
                        <button class="order_up"><img src="/resources/images/up_off.png" alt="오름차순"></button>
                        <button class="order_down"><img src="/resources/images/down_on.png" alt="내림차순"></button>
                    </div>
                </div> 
        </div>
        <div class="table_wrap">  
        <table>
                <thead>
                    <tr class="table_more">
                        <th class="number">NO.</th>
                        <th>지점 번호</th>
                        <th>지점명</th>
                        <th>대표자명</th>
                        <th>연락처</th>
                        <th>핸드폰 번호</th>
                        <th>주소</th>
                        <th>가맹점 수</th>
                        <th>매출</th>
                        <th>매출</th>
                        <th>사업자번호</th>
                        <th>수수료율</th>
                        <th>일꾼 수</th>
                    </tr>
                </thead>

                <tbody class="tbody">
                <%@ include file="/WEB-INF/views/common/default.jsp"%>
                </tbody>

		</table>
		</div>                  
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
    
    <!-- pop_up -->
    <div class="pop_up">
          <div class="pop_inner franchisee_pop center">
               <a href="#" class="close_btn"><img src="/resources/images/close_btn.png" class="center"/></a>
               <div class="top">
                    <div><span>지점 번호</span><input type="text" id="p_id" value="" disabled="disabled"></div>
                    <div><span>지점명</span><input type="text" id="p_name" value=""></div>
               </div>
               <ul class="pop_content">
                    <li class="section_1">
                        <div><span>대표자명</span><input type="text" id="p_ceo_name" value="" /></div>   
                        <div><span>연락처</span><input type="text" id="p_tel" value="" /></div>   
                        <div><span>핸드폰번호</span><input type="text" id="p_phone" value="" /></div> 
                   </li>
                   <li class="section_2">
                        <div><span>주소</span><input type="text" id="p_location" value="" /></div>   
                        <div><span>가맹점 수</span><input type="text" id="p_shop_cnt" value=""  disabled="disabled"/></div>   
                        <div><span>매출</span><input type="text" id="" value=""  disabled="disabled"/></div>   
                   </li>
                   <li class="section_3">
                        <div><span>사업자 번호</span><input type="text" id="p_business_code" value="" /></div>   
                        <div><span>수수료율</span><input type="text" id="" value=""  disabled="disabled"/></div>   
                        <div><span>일꾼수</span><input type="text" id="p_user_cnt" value=""  disabled="disabled"/></div>      
                   </li>
               </ul>
              <div class="button">
                 <button type="button" class="modify-btn" data-id="">수정</button>
                 <button type="button" class="delete-btn" data-id="">삭제</button>
                 <button type="button" class="file-pop-btn" id="p2_business" data-id="" data-column-name="business" data-table-name="branch">사업자 등록증</button>
              </div>
           </div>
       </div>
