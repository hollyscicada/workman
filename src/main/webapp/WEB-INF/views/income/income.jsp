<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
	<link rel="stylesheet" href="/resources/css/income.css" />
    <div class="content">
       <ul class="tab_menu">
           <li class="on" data-tab="tab1"><a href="#">본사 매출</a></li>
           <li data-tab="tab2"><a href="#">지사 매출</a></li>
       </ul>
       <!-- 본사 매출 -->
        <div class="tabcontent on" id="tab1">
                <div class="search_area">
                    <div class="top_more_btn">
                        <button class="blue_btn">일별 보기</button>
                        <button class="blue_btn">월별 보기</button>
                        <button class="blue_btn">분기별 보기</button>
                        <button class="blue_btn">연별 보기</button>
                    </div>
                    <div class="order_search">
                        <span>정렬</span>
                        <select id="" name="" class="option">
                            <option value="1">전체</option>
                            <option value="2">캐쉬 사용 금액</option>
                            <option value="3">캐쉬 남은 금액</option>
                            <option value="4">매장 회원비</option>
                            <option value="5">일꾼 회원비</option>
                            <option value="6">배정 합계</option>
                        </select>
                        <div class="order_btn">
                            <button class="order_up"><img src="/resources/images/up_off.png" alt="오름차순"></button>
                            <button class="order_down"><img src="/resources/images/down_on.png" alt="내림차순"></button>
                        </div>
                    </div>   
                    <div class="term_search">
                        <span>기간 검색</span>
                        <input type="date" name="dateofbirth">&nbsp;~&nbsp;
                        <input type="date" name="dateofbirth">
                        <div>
                            <input type="button" name="" id="" value="당일" class="opt_btn" />
                            <input type="button" name="" id="" value="1주" class="opt_btn" />
                            <input type="button" name="" id="" value="1달" class="opt_btn" />
                            <input type="button" name="" id="" value="검색" class="blue_btn">
                        </div>

                    </div>
                </div>      
                <table>
                <thead>
                    <tr>
                        <th class="number">날짜</th>
                        <th>캐쉬 충전 금액</th>
                        <th>캐쉬 사용 금액</th>
                        <th>캐쉬 남은 금액</th>
                        <th>매장 회원비</th>
                        <th>일꾼 회원비</th>
                        <th>배정 </th>
                    </tr>
                </thead>

                <tbody>
                    <tr id="" onclick="">
                        <td class="number">0000-00-00</td>
                        <td>000,000</td>
                        <td>000,000</td>
                        <td>000,000</td>
                        <td>000,000</td>
                        <td>000,000</td>
                        <td>0000</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>합계</td>
                        <td>000,0000</td>
                        <td>000,0000</td>
                        <td>000,0000</td>
                        <td>000,0000</td>
                        <td>000,0000</td>
                        <td>000,0000</td>
                    </tr>
                    <tr>
                        <td>총 매출</td>
                        <td colspan="6">000,000</td>
                    </tr>
                </tfoot>
            </table>
            </div>
        <!-- 지사 매출 -->
        <div class="tabcontent" id="tab2">
                <div class="search_area">
                    <div class="search">
                        <span>지사명 검색</span>
                        <div class="search_txt">
                            <input type="text" name="keyword" id="keyword" value="" />
                            <a href="#" class="search_btn"><img src="/resources/images/search_btn.png" class="center"></a>
                        </div>
                    </div>
                    <div class="order_search">
                        <span>정렬</span>
                        <select id="" name="" class="option">
                            <option value="1">전체</option>
                            <option value="2">캐쉬 사용 금액</option>
                            <option value="3">캐쉬 남은 금액</option>
                            <option value="4">매장 회원비</option>
                            <option value="5">일꾼 회원비</option>
                            <option value="6">배정 합계</option>
                        </select>
                        <div class="order_btn">
                            <button class="order_up"><img src="/resources/images/up_off.png" alt="오름차순"></button>
                            <button class="order_down"><img src="/resources/images/down_on.png" alt="내림차순"></button>
                        </div>
                    </div>   
                    <div class="term_search">
                        <span>기간 검색</span>
                        <input type="date" name="dateofbirth">&nbsp;~&nbsp;
                        <input type="date" name="dateofbirth">
                        <div>
                            <input type="button" name="" id="" value="당일" class="opt_btn" />
                            <input type="button" name="" id="" value="1주" class="opt_btn" />
                            <input type="button" name="" id="" value="1달" class="opt_btn" />
                            <input type="button" name="" id="" value="검색" class="blue_btn">
                        </div>

                    </div>
                </div>     
                <table>
                    <thead>
                        <tr>
                            <th class="number">날짜</th>
                            <th>지사명</th>
                            <th>캐쉬 충전 금액</th>
                            <th>캐쉬 사용 금액</th>
                            <th>캐쉬 남은 금액</th>
                            <th>매장 회원비</th>
                            <th>일꾼 회원비</th>
                            <th>배정 </th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr id="" onclick="">
                            <td class="number">0000-00-00</td>
                            <td>지사명</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>0000</td>
                        </tr>
                        <tr id="" onclick="">
                            <td class="number">0000-00-00</td>
                            <td>지사명</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>0000</td>
                        </tr>
                        <tr id="" onclick="">
                            <td class="number">0000-00-00</td>
                            <td>지사명</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>000,000</td>
                            <td>0000</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>합계</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                            <td>000,0000</td>
                        </tr>
                        <tr>
                            <td>총 매출</td>
                            <td colspan="7">000,000</td>
                        </tr>
                    </tfoot>
                </table>
             </div>                
    </div>
    <!-- 페이지 번호 -->          
    <%@ include file="/WEB-INF/views/page/paging.jsp"%>
