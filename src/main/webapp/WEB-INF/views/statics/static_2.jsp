<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp"%>
    <link rel="stylesheet" href="/resources/css/static.css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="/resources/js/chart.js"></script>
    <script>
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawCharts);
    function drawCharts() {
      // actual bar chart data
      var barData = google.visualization.arrayToDataTable([
        ['월별', '전체 매출'],
        ['1월',  1050],
        ['2월',  1370],
        ['3월',  1700],
        ['4월',  360],
        ['5월',  700],
        ['6월',  660],
        ['7월',  780],
        ['8월',  1200],
        ['9월',  340],
        ['10월',  120],
        ['11월',  500],
        ['12월',  1000]
      ]);
        
      // set bar chart options
      var barOptions = {
          /*차트 바*/
            focusTarget: 'category',
            backgroundColor: 'transparent',
            colors: ['#44a6fb'],
            chartArea: {
            left: 150,
            top: 50,
            width: '90%',
            height: '80%'
        },
        bar: {
            groupWidth: '50%'
        },
        hAxis: {
            textStyle: {
            fontSize: 12
          }
        },
          /*y축*/
        vAxis: {
              minValue: 0,
              maxValue: 2000,
              baselineColor: '#DDD',
              gridlines: {
                color: '#DDD',
                count: 6
              },
              textStyle: {
                fontSize: 12
              }
        },
          /*차트 구분*/
        legend: 'none'
      };
  // draw bar chart twice so it animates
  var barChart = new google.visualization.ColumnChart(document.getElementById('sales-chart1'));
  //barChart.draw(barZeroData, barOptions);
  barChart.draw(barData, barOptions);
        
}
    </script>
    </head>
    
    <div class="content">
       <ul class="tab_menu">
           <li><a href="/supervise/statics/1">회원수 통계</a></li>
           <li class="on"><a href="/supervise/statics/2">매출 통계</a></li>
       </ul>
        <!-- 매출 통계 -->
        <div class="tabcontent">  
            <!-- 전체/지점별 tab -->  
            <ul class="chart_menu">
                <li class="on" data-tab="tab1"><a href="#">전체 보기</a></li>
                <li data-tab="tab2"><a href="#">지점별 보기</a></li>
            </ul>
            <div class="content_inner">   
                <!-- 전체 -->
                <div class="tab_content on" id="tab1">
                   <table>
                        <thead>
                            <tr>
                                <th class="number">구분</th>
                                <th>1월</th>
                                <th>2월</th>
                                <th>3월</th>
                                <th>4월</th>
                                <th>5월</th>
                                <th>6월</th>
                                <th>7월</th>
                                <th>8월</th>
                                <th>9월</th>
                                <th>10월</th>
                                <th>11월</th>
                                <th>12월</th>
                                <th>총합</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr id="" onclick="">
                                <td class="number">전체 매출</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                                <td>0000</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="chart_wrap">
                       <div class="chart">
                            <div id="sales-chart1"></div>   
                       </div>
                    </div>
                </div>
                <!-- 지점별 -->
                <div class="tab_content" id="tab2">
                   <table>
                        <thead>
                            <tr>
                                <th class="number">구분</th>
                                <th>1월</th>
                                <th>2월</th>
                                <th>3월</th>
                                <th>4월</th>
                                <th>5월</th>
                                <th>6월</th>
                                <th>7월</th>
                                <th>8월</th>
                                <th>9월</th>
                                <th>10월</th>
                                <th>11월</th>
                                <th>12월</th>
                                <th>총합</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr id="" onclick="">
                                <td class="number">A지점</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>100</td>
                                <td>0000</td>
                            </tr>
                            <tr id="" onclick="">
                                <td class="number">B지점</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>200</td>
                                <td>0000</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr id="" onclick="">
                                <td class="number">합계</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>300</td>
                                <td>0000</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="chart_wrap spot_chart">
                       <div class="chart">
                            <div id="sales-chart2"></div>   
                       </div>
                    </div> 
                </div>
            </div> 
        </div>       
              
    </div>

