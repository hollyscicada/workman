$(function(){
    
    /* gnb */
    $('.menu_left > li').click( function(){
        
        $('.menu_left > li').removeClass('active');
        $('.menu_right > li').removeClass('active');
        $(this).addClass('active');
        
    });
    $('.menu_right > li').click( function(){
        
        $('.menu_left > li').removeClass('active');
        $('.menu_right > li').removeClass('active');
        $(this).addClass('active');
        
    });
    
    
    /* 페이지번호 */
    $( '.page_num>a' ).click( function() {
    
        $('.page_num>a').removeClass('active')
        $(this).addClass('active');
 
    });
    /* 정렬 검색 버튼 */
    $('.order_btn > .order_down').click( function(e){
        var $this = $(this);
        var $order_up = $('.order_btn > .order_up');
        
         $order_up.find("img").attr("src" , function(index, attr){
             if(attr.match('_on')){
                return attr.replace("_on.png" , "_off.png");
            }
         });
        
        $this.find("img").attr("src" , function(index, attr){
            if(attr.match('_on')){
                return attr.replace("_on.png" , "_off.png");
            }else{
                return attr.replace("_off.png" , "_on.png");
            }
        });
    });
    $('.order_btn > .order_up').click( function(e){
        var $this = $(this);
        var $order_down = $('.order_btn > .order_down');
        
         $order_down.find("img").attr("src" , function(index, attr){
             if(attr.match('_on')){
                return attr.replace("_on.png" , "_off.png");
            }
         });
        
        $this.find("img").attr("src" , function(index, attr){
            if(attr.match('_off')){
                return attr.replace("_off.png" , "_on.png");
            }else{
                return attr.replace("_on.png" , "_off.png");
            }
        });
    });
    
    /* 기간검색 달력 */
/*    
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });*/
    
    /*회원가입 승인버튼 - pop*/
    $('.join_submit').click(function(e){
        e.preventDefault;
        
        $('.pop_inner').hide();
        $('.join_pop').show();
        
    });
    
    /* 매출 탭메뉴 */
        $('.tab_menu > li > a').click(function(e) {
            e.preventDefault;
            
            var activeTab = $(this).parent().attr('data-tab');


            $('.tab_menu>li').removeClass('on');
            $('.tabcontent').removeClass('on');

            $(this).parent().addClass('on');
            $('#' + activeTab).addClass('on');


        });
    /* 통계 탭메뉴 */
        $('.chart_menu > li > a').click(function(e) {
            e.preventDefault;
            
            var activeTab = $(this).parent().attr('data-tab');


            $('.chart_menu>li').removeClass('on');
            $('.tab_content').removeClass('on');

            $(this).parent().addClass('on');
            $('#' + activeTab).addClass('on');

        });
    
    /* 메세지 보내기 검색버튼 */
        $('.mess_top_btn > button').click(function() {

            $('.mess_top_btn > button').removeClass('on');
            $(this).addClass('on');


        });
    
    /* pop up */
     $(document).on("dblclick", '.content > table > tbody > tr', function() {
//        $('.pop_up').css({"display" : "block"});
    });
    $( '.close_btn' ).click( function() {
    
        $('.pop_up').css({"display" : "none"});
 
    });
    
        /* punish up */
     $( 'button.punish' ).click( function() {
    
        $('.punish_pop').css({"display" : "block"});
        $('.pop_back').css({"display" : "block"});
 
    });
     $( 'button.out' ).click( function() {
    
        $('.out_pop').css({"display" : "block"});
        $('.pop_back').css({"display" : "block"});
 
    });
    $( '.close_btn' ).click( function() {
    
        $('.punish_pop').css({"display" : "none"});
        $('.out_pop').css({"display" : "none"});
        $('.pop_back').css({"display" : "none"});
        $('.long_term_pop').css({"display" : "none"});
        $('.reply_pop').css({"display" : "none"});
 
    });
    
    $('.inn_pop_close').click(function(){
        $(this).parent().css({"display" : "none"});
        $('.pop_inner').css({"display" : "block"});
        
    });
    $('.long_term').click(function(){
        $('.long_term_pop').css({"display" : "block"});
    });
    
    /* 요청-매장정보보기 */
//    $('.shop_info').click(function(){
//        $('.shop_info_pop').css({"display" : "block"});
//        $('.reauest_pop').css({"display" : "none"});
//    });
    $('.shop_info_close').click(function(){
        $('.shop_info_pop').css({"display" : "none"});
        $('.reauest_pop').css({"display" : "flex"});
    });
    
    /* 요청-배정 ------------------------------------------------*/
    $('.assignment_pop_close').click(function(){
        
        $('.reauest_pop').css({"display" : "flex"});
        $('.assignment_pop').css({"display" : "none"});
        
    });
    
    /* 메세지 답장하기 버튼 ----------------------------------------*/
    $( '.reply_btn > button' ).click( function() {
    
        $('.reauest_pop').css({"display" : "none"});
        $('.assignment_pop').css({"display" : "block"});
 
    });
    
    $( '.file-pop-btn' ).click( function() {
    	
    	var table_name = $(this).attr("data-table-name");
    	var column_name = $(this).attr("data-column-name");
    	var id = $(this).attr("data-id");
    	window.open("/supervise/common/popup?table_name="+table_name+"&column_name="+column_name+"&id="+id, '', 'width=600, height=636');
    	
    });
    
    
    /*메세지 보내기 파일 찾기*/
    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
      customInput(inputs[i])
    }

    function customInput (el) {
      const fileInput = el.querySelector('[type="file"]')
      const label = el.querySelector('[data-js-label]')

	      fileInput.onchange =
	      fileInput.onmouseout = function () {
	        if (!fileInput.value) return
	
	        var value = fileInput.value.replace(/^.*[\\\/]/, '')
	        el.className += ' -chosen'
	        label.innerText = value
	    }
    }
})  

    var global = {
    		job_format:function(val){
    	        var job_names = "";
    	            if(val || val == 0){
    	                job_names = globalJobArray[val]
    	            }
    	
    	          return job_names;
    		},
    		job_format_multi:function(val){
    			var job_names = "";
                if(val && val != '[]' && val != 'undefined' && val.indexOf('null')<=-1){
                    var job_json = JSON.parse(val);

                    for(var i = 0 ; i < job_json.length ; i++){
                        var index = job_json[i];
                        var job_name = globalJobArray[index];
                        job_names += job_name;

                        if(i != job_json.length-1){
                            job_names += ", ";
                        }
                    }
                }

              return job_names;
    		},
    		comma:function(val){
    			val = String(val);
                return val.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    		},
    		hipen:function(val){
    			if(val){
    				val = String(val);
    				return val.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
    			}else{
    				return "";
    			}
    		},
    }
function previewImgCreateFront(e) {
    var $input = $(this);
    var inputFile = this.files[0];
    
    var reader = new FileReader();
    reader.onload = function(event) {
        ajaxUpload(inputFile, $input, function(data_url){
        	$input.next().val(data_url);
        	$(".file_url_text").val(data_url)
        })
    };
    reader.readAsDataURL(inputFile);
}
function previewImgCreateFront2(e) {
	var $input = $(this);
	var inputFile = this.files[0];
	
	var reader = new FileReader();
	reader.onload = function(event) {
		ajaxUpload(inputFile, $input, function(data_url){
			$("#target_img").attr("src", data_url);
			$input.next().val(data_url);
		})
	};
	reader.readAsDataURL(inputFile);
}
function ajaxUpload(inputFile, $input, callback){
	var formData = new FormData();
	formData.append("file", inputFile);
	jQuery.ajax({
		url : "/api/file/ajaxupload"
			, type : "POST"
				, processData : false
				, contentType : false
				, data : formData
				, success:function(data_url) {
					callback(data_url)
				}
	});
}
